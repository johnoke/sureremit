package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/2/2016.
 */
public class StripePaymentHelper {
    public String stripeToken;
    public double amount;
    public StripePaymentHelper(String stripeToken, double amount){
        this.stripeToken = stripeToken;
        this.amount = amount;
    }
}
