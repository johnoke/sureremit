package com.suregifts.sureremit;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Signup extends AppCompatActivity {

    private EditText inputEmail, inputPassword;
    private Button btnSignIn, btnSignUp, btnResetPassword;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private String generateWalletResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        btnSignIn = (Button) findViewById(R.id.sign_in_button);
        btnSignUp = (Button) findViewById(R.id.sign_up_button);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnResetPassword = (Button) findViewById(R.id.btn_reset_password);

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Signup.this, ResetPasswordActivity.class));
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                //create user
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(Signup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(Signup.this, "Email already exist", Toast.LENGTH_SHORT).show();
                                } else {
                                    new AsyncTaskPostCreateWallet().execute();
                                    startActivity(new Intent(Signup.this, MainActivity.class));
                                    finish();
                                }
                            }
                        });

            }
        });
    }
    public String GenerateKey(){
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int count = 26;
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
    public class AsyncTaskPostCreateWallet extends AsyncTask<String, Void, Void>
    {
        @Override
        protected void onPostExecute(Void aVoid) {
            try {

                if (generateWalletResult.contains("success")){
                    Gson gson = new GsonBuilder().create();
                    try {
                        JSONObject obj = new JSONObject(generateWalletResult);
                        String publicKey = obj.getString("publicKey");
                        String secretKey = obj.getString("secretKey");
                        String walletData = "{\"PublicKey\":\"" + publicKey + "\", \"SecretKey\":\"" + secretKey + "\"}";
                        FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(walletData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                    }
                    catch (Exception ex)
                    {
                        String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                        FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(walletData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                    }
                }
                else{
                    try{
                        String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                        FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(walletData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        outputStreamWriter.close();
                    }catch (Exception e){}
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                try{
                    String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                    FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(walletData);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    outputStreamWriter.close();
                }catch (Exception e){}
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("https://sr-stellar.herokuapp.com/account/create")
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                generateWalletResult = response.body().string();
            }
            catch(IOException ex)
            {
                generateWalletResult = "Server Error";
            }
            return null;
        }
    }
}
