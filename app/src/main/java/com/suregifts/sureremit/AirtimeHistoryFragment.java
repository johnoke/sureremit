package com.suregifts.sureremit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AirtimeHistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    String historyResponse;
    ProgressDialog progressDialog;
    UserProfile userProfile;
    LinearLayout voucherHistoryContainer;
    List<VoucherHistory> airtimeHistories;
    String operation = "";
    public AirtimeHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AirtimeHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AirtimeHistoryFragment newInstance(String param1, String param2) {
        AirtimeHistoryFragment fragment = new AirtimeHistoryFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userProfile = new UserProfile("","","","","");

        airtimeHistories = new ArrayList<VoucherHistory>();

        GetUserData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_vouchers_history, container, false);
        voucherHistoryContainer = (LinearLayout)v.findViewById(R.id.HistoryList);
        if (airtimeHistories.size() < 1){
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Fetching History...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            new FetchAllHistory().execute();
        }
        else{
            DisplayHistory();
        }
        return v;
    }

    public class FetchHistory extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            BindHistory();
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String merchantUrl = "";
            if(operation.equals("Voucher")) {
                merchantUrl = "http://sureremit.co/api/voucher/history?email=" + userProfile.Email;
            }
            else if(operation.equals("Airtime")) {
                merchantUrl = "http://sureremit.co/api/airtime/history?email=" + userProfile.Email;
            }
            else {
                merchantUrl = "http://sureremit.co/api/bill/history?email=" + userProfile.Email;
            }
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();


            Response response;
            try{
                response = client.newCall(request).execute();
                historyResponse = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
    public void DisplayHistory(){
        if(((LinearLayout) voucherHistoryContainer).getChildCount() > 0)
            ((LinearLayout) voucherHistoryContainer).removeAllViews();
        for(int i = airtimeHistories.size() - 1; i >= 0; i--){
            final VoucherHistory history = airtimeHistories.get(i);
            try{
                LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View historyView = layoutInflater.inflate(R.layout.historycard, null);
                TextView merchantName = (TextView)historyView.findViewById(R.id.MerchantName);
                TextView recipient = (TextView)historyView.findViewById(R.id.Recipient);
                TextView category = (TextView)historyView.findViewById(R.id.MerchantCategory);
                TextView amountpayable = (TextView)historyView.findViewById(R.id.AmountPayable);
                ImageView country = (ImageView)historyView.findViewById(R.id.CountryFlag);
                TextView amount = (TextView)historyView.findViewById(R.id.Amount);
                TextView date = (TextView)historyView.findViewById(R.id.Date);
                TextView reorderIcon = (TextView)historyView.findViewById(R.id.ResendBtn);
                ImageView merchantImage = (ImageView)historyView.findViewById(R.id.MerchantImage);
                reorderIcon.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    VoucherHelper voucherHelper = new VoucherHelper();
                                    voucherHelper.category_id = 0;
                                    voucherHelper.category = "";
                                    voucherHelper.merchant_id = history.merchantId == null ? 0 : Integer.parseInt(history.merchantId);
                                    voucherHelper.merchant = history.service;
                                    voucherHelper.merchant_picture = history.merchantImage;
                                    voucherHelper.location = history.location;
                                    voucherHelper.amount = history.amount;
                                    voucherHelper.amount_payable = history.amountPayable;
                                    voucherHelper.recipient_name = history.recipientName;
                                    voucherHelper.recipient_email = history.recipientEmail;
                                    voucherHelper.recipient_phone = history.recipientPhone;
                                    voucherHelper.email = history.senderEmail;
                                    voucherHelper.payment_id = "";
                                    voucherHelper.country = history.location.equals("nigeria") ? "NG" : "KE";
                                    voucherHelper.location = voucherHelper.country;
                                    voucherHelper.category = "Airtime";
                                    voucherHelper.optype = "Airtime";
                                    voucherHelper.code = history.serviceId;
                                    Intent intent = new Intent(getContext(), RecipientDetailsActivity.class);
                                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                                    startActivity(intent);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                            }
                        }
                );
                if (history.location.equals("nigeria")){
                    country.setImageResource(R.drawable.nigerian_flag);
                }else if(history.location.equals("kenya")){
                    country.setImageResource(R.drawable.kenyan_flag);
                }else{
                    country.setImageResource(R.drawable.rwandan_flag);
                }

                merchantName.setText(history.service);
                recipient.setText(history.recipientPhone);
                amountpayable.setText("$ " +history.amountPayable);
                amount.setText(history.currency + " " + history.amount);
                category.setText(history.category);

                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat newformatter = new SimpleDateFormat("dd MMM yyyy");
                    String[] dateData = history.createdAt.split("T");
                    String dateToDisplay = dateData[0];
                    Date newdate = formatter.parse(dateData[0]);
                    dateToDisplay = newformatter.format(newdate);
                    date.setText(dateToDisplay);
                }catch (Exception e){
                    date.setText(history.createdAt);
                }

                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.imageView = merchantImage;
                viewHolder.imageUrl = history.merchantImage;
                new DownloadAsyncTask(viewHolder).execute(viewHolder);
                voucherHistoryContainer.addView(historyView);

            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

    }
    public void BindHistory(){
        Gson gson = new GsonBuilder().create();
        try{
            JSONObject jsonObject = new JSONObject(historyResponse);
            JSONArray jsonArray = jsonObject.getJSONArray("airtimes");
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject voucher = jsonArray.getJSONObject(i);
                String voucherStr = voucher.toString();
                VoucherHistory voucherHistory = gson.fromJson(voucherStr, VoucherHistory.class);
                airtimeHistories.add(voucherHistory);
                voucherHistory.amountPayable = String.valueOf(Double.valueOf(voucherHistory.amountPayable) / 100);
            }
            DisplayHistory();
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }
    public void GetUserData()
    {
        try{
            String rawdata = new Scanner(new File(getActivity().getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            userProfile.Email = obj.getString("Email");
            userProfile.FirstName = obj.getString("FirstName");
            userProfile.LastName = obj.getString("LastName");
            userProfile.PhoneNumber = obj.getString("Phone");
            String names = obj.getString("Name");
            try{
                String[] FullNames = names.split(" ");
                userProfile.LastName = FullNames[1];
            }
            catch(Exception ex){
                userProfile.LastName = "";
            }
        }
        catch (Exception ex){

        }

    }
    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        private int position;
        public DownloadAsyncTask(ViewHolder viewHolder)
        {

        }
        @Override
        protected  ViewHolder doInBackground(ViewHolder... params){
            ViewHolder viewHolder = params[0];

            try{
                URL imageUrl = new URL(viewHolder.imageUrl);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageUrl.openStream());
            }catch(IOException e){
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }
        @Override
        protected void onPostExecute(ViewHolder result){
            if (result.bitmap == null){
                result.imageView.setImageResource(android.R.color.transparent);
            }else{
                result.imageView.setImageBitmap(result.bitmap);
            }

        }
    }
    private static class ViewHolder {
        ImageView imageView;
        TextView name;
        String datetime;
        String imageUrl;
        Bitmap bitmap;
        int id;
    }
    public class FetchAllHistory extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            BindHistory();
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String merchantUrl = "http://sureremit.co/api/airtime/history?email=" + userProfile.Email;
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();

            Response response;
            try{
                response = client.newCall(request).execute();
                historyResponse = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }

}
