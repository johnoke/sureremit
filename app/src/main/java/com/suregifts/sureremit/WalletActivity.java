package com.suregifts.sureremit;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WalletActivity extends AppCompatActivity {
    Toolbar mainToolBar;
    String generateWalletResult;
    String fetchWalletBalance;
    String storedWalletObject;
    FontRegularTextView walletAddressView, rmtBalanceView, dollarEquiv;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        mainToolBar = (Toolbar)findViewById(R.id.ProfileToolbar);
        setTitle("");
        setSupportActionBar(mainToolBar);
        generateWalletResult = "";
        fetchWalletBalance = "";
        walletAddressView = findViewById(R.id.walletAddress);
        rmtBalanceView = findViewById(R.id.RMTBalance);
        dollarEquiv = findViewById(R.id.DollarEquiv);
        storedWalletObject = "";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        File file = new File(getFilesDir() + "/sureremit_wallet_cache.txt");
        if (file.exists()){
            try{
                storedWalletObject = new Scanner(file).nextLine();
                LoadWallet();
            }catch (Exception ex){
                progressDialog.dismiss();
            }

        }else{
            new AsyncTaskPostCreateWallet().execute();
        }
    }
    public void LoadWallet(){
        try{
            JSONObject jsonObject = new JSONObject(storedWalletObject);
            String walletAddress = jsonObject.getString("PublicKey");
            walletAddressView.setText(walletAddress);
            new FetchWalletDetails().execute();
            progressDialog.dismiss();
        }catch (Exception e){
            progressDialog.dismiss();
        }
    }
    public String GenerateKey(){
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int count = 26;
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            default:
                finish();
                return true;
        }
        // return super.onOptionsItemSelected(item);
    }
    public class FetchWalletDetails extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            try {
                JSONObject jsonObject = new JSONObject(fetchWalletBalance);
                JSONArray balances = jsonObject.getJSONArray("balances");
                JSONObject remitJSONObject = balances.getJSONObject(0);
                JSONObject dollarsSONObject = balances.getJSONObject(1);
                rmtBalanceView.setText("RMT" + remitJSONObject.getString("balance"));
                dollarEquiv.setText("$" + dollarsSONObject.getString("balance"));
                progressDialog.dismiss();
            }
            catch (Exception ex)
            {
                rmtBalanceView.setText("RMT100,000");
                dollarEquiv.setText("$2000");
                progressDialog.dismiss();
                ex.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String merchantUrl = "https://sr-stellar.herokuapp.com/account/balance/";
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();

            Response response;
            try{
                response = client.newCall(request).execute();
                fetchWalletBalance = response.body().string();
            }
            catch(Exception ex){
                rmtBalanceView.setText("RMT100,000");
                dollarEquiv.setText("$2000");
                progressDialog.dismiss();
            }
            return null;
        }
    }
    public class AsyncTaskPostCreateWallet extends AsyncTask<String, Void, Void>
    {
        @Override
        protected void onPostExecute(Void aVoid) {
            try {

                if (generateWalletResult.contains("success")){
                    Gson gson = new GsonBuilder().create();
                    try {
                        JSONObject obj = new JSONObject(generateWalletResult);
                        String publicKey = obj.getString("publicKey");
                        String secretKey = obj.getString("secretKey");
                        String walletData = "{\"PublicKey\":\"" + publicKey + "\", \"SecretKey\":\"" + secretKey + "\"}";
                        FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(walletData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        storedWalletObject = generateWalletResult;
                        progressDialog.dismiss();
                        LoadWallet();
                    }
                    catch (Exception ex)
                    {
                        progressDialog.dismiss();
                        ex.printStackTrace();
                    }
                }
                else{
                    try{
                        String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                        FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(walletData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        outputStreamWriter.close();
                        progressDialog.dismiss();
                        LoadWallet();
                    }catch (Exception e){}
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                try{
                    String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                    FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(walletData);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    outputStreamWriter.close();
                    progressDialog.dismiss();
                    LoadWallet();
                }catch (Exception e){}
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("https://sr-stellar.herokuapp.com/account/create")
                        .get()
                        .build();
                Response response = client.newCall(request).execute();
                generateWalletResult = response.body().string();
            }
            catch(IOException ex)
            {
                generateWalletResult = "Server Error";
                try{
                    String walletData = "{\"PublicKey\":\"" + GenerateKey() + "\", \"SecretKey\":\"" + GenerateKey() + "\"}";
                    FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sureremit_wallet_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(walletData);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    outputStreamWriter.close();
                }catch (Exception e){}
                progressDialog.dismiss();
                LoadWallet();
            }
            return null;
        }
    }
}
