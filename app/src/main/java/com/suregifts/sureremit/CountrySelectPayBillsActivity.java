package com.suregifts.sureremit;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v4.widget.DrawerLayout;
import java.io.File;

public class CountrySelectPayBillsActivity extends AppCompatActivity {
    Toolbar countryPayBillToolbar;
    TextView payBillCountrySelectText, nigeriaText, kenyaText, payBillsTitle;
    private DrawerLayout menuDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_select_pay_bills);
        setTitle("");
        countryPayBillToolbar = (Toolbar)findViewById(R.id.CountryPayBillsToolbar);
        setSupportActionBar(countryPayBillToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        payBillCountrySelectText = (TextView) findViewById(R.id.VoucherSelectCountryText);
        payBillCountrySelectText.setText("Select the country where you \nwant to pay a bill ");
        Typeface font = Typeface.createFromAsset(getAssets(), "Lato-BoldItalic.ttf");
        Typeface fontRegular = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        Typeface fontBold = Typeface.createFromAsset(getAssets(), "Lato-Bold.ttf");
        payBillCountrySelectText.setTypeface(font);
        nigeriaText = (TextView)findViewById(R.id.PayBillsNigeriaText);
        kenyaText = (TextView)findViewById(R.id.PayBillsKenyaText);
        payBillsTitle = (TextView)findViewById(R.id.payBillsTitle);
        nigeriaText.setTypeface(fontRegular);
        kenyaText.setTypeface(fontRegular);
        payBillsTitle.setTypeface(fontBold);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public void LaunchBillsNG(View view)
    {
        Intent intent = new Intent(this, BillsCategoryActivity.class);
        String country = "NG";
        intent.putExtra("country", country);
        startActivity(intent);
    }
    public void LaunchBillsKE(View view)
    {
        Intent intent = new Intent(this, BillsCategoryActivity.class);
        String country = "KE";
        intent.putExtra("country", country);
        startActivity(intent);
    }
    public void LaunchBillsRW(View view)
    {
        Intent intent = new Intent(this, BillsCategoryActivity.class);
        String country = "RW";
        intent.putExtra("country", country);
        startActivity(intent);
    }
}
