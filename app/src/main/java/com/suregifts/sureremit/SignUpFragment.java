package com.suregifts.sureremit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignUpFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FontRegularTextView signupErrMessage;
    private EditText email, firstname, lastname, password, phonenumber;
    private TextInputLayout emailLayout, firstnameLayout, lastnameLayout, passwordLayout, phonenumberLayout;
    private AppCompatButton submitButton;
    private CallbackManager callbackManager;
    private String signupResult;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    AccessToken accessToken;
    private String loginResult;
    UserProfile userProfile;
    ProgressDialog progressDialog;
    UserHelper userHelper;
    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }
    @Override
    public void onResume(){
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //nextActivity(profile);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View v = inflater.inflate(R.layout.fragment_sign_up, container, false);
        email = (EditText)v.findViewById(R.id.SignUpEmail);
        firstname = (EditText)v.findViewById(R.id.SignUpFirstName);
        lastname = (EditText)v.findViewById(R.id.SignUpLastName);
        password = (EditText)v.findViewById(R.id.SignUpPassword);
        phonenumber = (EditText)v.findViewById(R.id.SignUpPhone);
        emailLayout = (TextInputLayout)v.findViewById(R.id.SignUpEmailLayout);
        firstnameLayout = (TextInputLayout)v.findViewById(R.id.SignUpFirstNameLayout);
        submitButton = (AppCompatButton)v.findViewById(R.id.SignUpButton);
        lastnameLayout = (TextInputLayout)v.findViewById(R.id.SignupLastNameLayout);
        phonenumberLayout = (TextInputLayout)v.findViewById(R.id.SignUpPhoneLayout);
        passwordLayout = (TextInputLayout)v.findViewById(R.id.SignUpPasswordLayout);
        email.addTextChangedListener(new MyTextWatcher(email));
        firstname.addTextChangedListener(new MyTextWatcher(firstname));
        lastname.addTextChangedListener(new MyTextWatcher(lastname));
        password.addTextChangedListener(new MyTextWatcher(password));
        phonenumber.addTextChangedListener(new MyTextWatcher(phonenumber));
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Submit();
            }
        });
        callbackManager = CallbackManager.Factory.create();
        progressDialog = new ProgressDialog(getActivity());
        signupErrMessage = (FontRegularTextView)v.findViewById(R.id.SignUpErrMsg);
        callbackManager = CallbackManager.Factory.create();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //  nextActivity(newProfile);
            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
        profileTracker.startTracking();
        LoginButton loginButton = (LoginButton)v.findViewById(R.id.SignUpFBButton);
        callbackManager = CallbackManager.Factory.create();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //  nextActivity(newProfile);
            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
        profileTracker.startTracking();
        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try{
                            String uid = loginResult.getAccessToken().getUserId().toString();
                            String email = object.getString("email");
                            String name = object.getString("name");
                            String[] names = name.split(" ");
                            String firstname =  "";
                            String lastname = "";
                            try{
                                firstname = names[0];
                                lastname = names[1];
                            }catch(Exception ex){
                                firstname = name;
                            }
                            userHelper = new UserHelper(uid, email, firstname, lastname);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setCancelable(false);
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();
                            new AsyncFacebookCallBack().execute();
                        }catch(Exception ex){
                            ex.printStackTrace();
                            DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        // Inflate the layout for this fragment
        return v;
    }
    public class AsyncFacebookCallBack extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {
            String result;
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(userHelper);
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/auth/facebook")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
            }
            catch(IOException ex)
            {
                result = "Server Error";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject obj = new JSONObject(s);
                String status = obj.getString("status");
                if(status.equals("0")){
                    String phone = "";
                    UserProfile userProfile = new UserProfile(userHelper.firstname, userHelper.lastname, userHelper.email, "", phone);
                    Gson gson = new GsonBuilder().create();
                    String userData = gson.toJson(userProfile);
                    try {
                        FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(userData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        LaunchMenu();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        loginResult = "Internal App Error";
                        DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                    }
                } else {
                    DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                }
            } catch (Exception ex) {
                DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
            }
            LoginManager.getInstance().logOut();
            progressDialog.dismiss();
        }
    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.SignUpEmail:
                    validateEmail();
                    break;
                case R.id.SignUpPassword:
                    validatePassword();
                    break;
                case R.id.SignUpFirstName:
                    validateFirstName();
                    break;
                case R.id.SignUpLastName:
                    validateLastName();
                    break;
                case R.id.SignUpPhone:
                    validatePhone();
                    break;
            }
        }
    }
    private boolean validateEmail(){
        String strEmail = email.getText().toString();
        String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        boolean validateEmail = strEmail.matches(EMAIL_REGEX);
        if (strEmail.equals("") || strEmail.equals(null)) {
            emailLayout.setError("Email is required");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        }else if(!validateEmail){
            emailLayout.setError("Email is not in the right format");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        }
        else {
            emailLayout.setError(null);
        }

        return true;
    }
    private boolean validatePassword(){
        String strPassword = password.getText().toString();
        if (strPassword.equals("") || strPassword.equals(null)) {
            passwordLayout.setError("Password is required");
            passwordLayout.requestFocus();
            passwordLayout.setErrorEnabled(true);
            return false;
        } else {
            passwordLayout.setError(null);
        }

        return true;
    }
    private boolean validatePhone(){
        String strPhone = phonenumber.getText().toString();
        if (strPhone.equals("") || strPhone.equals(null)) {
            phonenumberLayout.setError("Phone Number is required");
            phonenumberLayout.requestFocus();
            phonenumberLayout.setErrorEnabled(true);
            return false;
        } else {
            phonenumberLayout.setError(null);
        }

        return true;
    }
    private boolean validateFirstName(){
        String strFirstName = firstname.getText().toString();
        if (strFirstName.equals("") || strFirstName.equals(null)) {
            firstnameLayout.setError("First Name is required");
            firstnameLayout.requestFocus();
            firstnameLayout .setErrorEnabled(true);
            return false;
        } else {
            firstnameLayout.setError(null);
        }

        return true;
    }
    private boolean validateLastName(){
        String strLastName = lastname.getText().toString();
        if (strLastName.equals("") || strLastName.equals(null)) {
            lastnameLayout.setError("Last Name is required");
            lastnameLayout.requestFocus();
            lastnameLayout.setErrorEnabled(true);
            return false;
        } else {
            lastnameLayout.setError(null);
        }

        return true;
    }
    public void Submit()
    {
        if(validateEmail() && validateFirstName() && validateLastName() && validatePhone() && validatePassword())
        {
            userProfile = new UserProfile(firstname.getText().toString(), lastname.getText().toString(), email.getText().toString(), password.getText().toString(), phonenumber.getText().toString() );
            try
            {
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Signing Up...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                AsyncTaskPostSignUp asyncTaskPostSignUp = new AsyncTaskPostSignUp();
                asyncTaskPostSignUp.execute();
            }
            catch (Exception ex)
            {
                signupResult = "An Error has occurred";
                Display(signupResult);
            }
        }
    }
    public class AsyncTaskPostSignUp extends AsyncTask<String, Void, Void>
    {
        @Override
        protected void onPostExecute(Void aVoid) {
            try {

                if (signupResult.contains("token")){
                    Gson gson = new GsonBuilder().create();
                    String userData = gson.toJson(userProfile);
                    try {
                        FileOutputStream fileOutputStream = getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(userData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        LaunchMenu();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        signupResult = "Internal App Error";
                        Display(signupResult);
                    }

                }
                else{
                    JSONObject obj = new JSONObject(signupResult);
                    String loginapimsg = obj.getString("message");
                    Display(loginapimsg);
                }

            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                signupResult = "Internal Server Error";
                Display(signupResult);
            }
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String json = "{\"email\" : \"" + userProfile.Email + "\", \"password\" : \"" + userProfile.Password + "\", \"fullname\" : \"" +  userProfile.FirstName + " " + userProfile.LastName +  "\", \"phone\" : \"" + userProfile.PhoneNumber + "\" }";
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/signup")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                signupResult = response.body().string();
            }
            catch(IOException ex)
            {
                signupResult = "Server Error";
            }
            return null;
        }
    }
    public void Display(String errMsg)
    {
        signupErrMessage.setText(errMsg);
        signupErrMessage.setVisibility(View.VISIBLE);
    }
    public void LaunchMenu()
    {
        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
        accessTokenTracker.stopTracking();
    }
    public void DisplayErrorPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        accessTokenTracker.stopTracking();
    }

}
