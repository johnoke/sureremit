package com.suregifts.sureremit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SelectMerchant extends AppCompatActivity {
    private DrawerLayout menuDrawer;
    Toolbar toolbar;
    String merchantsJson;
    VoucherHelper voucherHelper;

    List<Merchant> merchantList;
    GridLayout container;
    ProgressDialog progressDialog;
    String fileMerchantsName;
    List<Merchant> recentlyUsedMerchants;
    LinearLayout recentlyUsedTab, recentlyUsedContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_merchant);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
        Intent intent = getIntent();
        voucherHelper = (VoucherHelper) intent.getSerializableExtra("voucherHelper");
        merchantsJson = intent.getStringExtra("categoriesLoaded");
        merchantList = new ArrayList<Merchant>();
        container = (GridLayout)findViewById(R.id.MerchantsGrid);
        recentlyUsedContainer = (LinearLayout)findViewById(R.id.RecentlyUsedContainer);
        recentlyUsedTab = (LinearLayout)findViewById(R.id.RecentlyUsedTab);
        recentlyUsedMerchants = Merchant.VoucherUsedList(this, voucherHelper.country);
        progressDialog = new ProgressDialog(SelectMerchant.this);
        setTitle("");

        FetchAllMerchants merchantsLoader = new FetchAllMerchants();
       // merchantsLoader.execute();

        if (recentlyUsedMerchants.size() > 0){
            BindRecentlyUsedMerchants();
            recentlyUsedTab.setVisibility(View.VISIBLE);
        }
        DisplayMerchants();
    }
    public void BindRecentlyUsedMerchants(){
        for(int i = recentlyUsedMerchants.size() - 1; i >= 0; i--){
            final Merchant merchant = recentlyUsedMerchants.get(i);
            LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View merchantView = layoutInflater.inflate(R.layout.billsdisplay, null);
            CardView merchantContainer = (CardView)merchantView.findViewById(R.id.MerchantContainer);
            ImageView merchantImage = (ImageView)merchantView.findViewById(R.id.MerchantImage);
            TextView merchantText = (TextView)merchantView.findViewById(R.id.MerchantName);
            merchantText.setText(merchant.MerchantName);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageUrl = merchant.ImageName;
            viewHolder.imageView =  merchantImage;
            new DownloadAsyncTask(viewHolder).execute(viewHolder);
            merchantContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voucherHelper.setMerchant_id(merchant.MerchantId);
                    voucherHelper.setMerchant(merchant.MerchantName);
                    voucherHelper.setMerchant_picture(viewHolder.imageUrl);
                    Intent intent = new Intent(SelectMerchant.this, RecipientDetailsActivity.class);
                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                    startActivity(intent);
                }
            });

            recentlyUsedContainer.addView(merchantView);
        }
    }
    public void DisplayMerchants()
    {
        try{
            JSONArray jsonArray = new JSONArray(merchantsJson);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt("categoryid");
                if (id == voucherHelper.category_id){
                    JSONArray merchantsArray = jsonObject.getJSONArray("merchants");
                    for (int j = 0; j < merchantsArray.length(); j++){
                        JSONObject merchantObject = merchantsArray.getJSONObject(j);
                        String merchantname = merchantObject.getString("MerchantName");
                        int merchantId = merchantObject.getInt("MerchantId");
                        String imageUrl = merchantObject.getString("ImageUrl");
                        String description = merchantObject.getString("Description");
                        String shortDescription = merchantObject.getString("ShortDescription");
                        String redemptionDetails = "";
                        String locations = "";
                        try{
                            JSONArray redemptionsArray = merchantObject.getJSONArray("RedemptionDetails");
                            for (int k = 0; k < redemptionsArray.length(); k++){
                                String detail = redemptionsArray.getString(k);
                                redemptionDetails += detail;
                                redemptionDetails += "\n";
                            }
                        }catch(Exception ex){

                        }
                        try{
                            JSONArray locationsArray = merchantObject.getJSONArray("Locations");
                            for (int k = 0; k < locationsArray.length(); k++){
                                String location = locationsArray.getString(k);
                                locations += location;
                                locations += "\n";
                            }
                        }catch(Exception ex){

                        }

                        Merchant merchant = new Merchant(merchantId, merchantname, imageUrl, id, shortDescription, description);
                        merchant.setRedemptionDetails(redemptionDetails);
                        merchant.setLocations(locations);
                        merchantList.add(merchant);
                    }
                }
            }
            BindMerchants();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void BindMerchants(){
        for(final Merchant merchant:merchantList){
            LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View merchantView = layoutInflater.inflate(R.layout.merchantdisplay, null);
            CardView merchantContainer = (CardView)merchantView.findViewById(R.id.MerchantContainer);
            ImageView merchantImage = (ImageView)merchantView.findViewById(R.id.MerchantImage);
            TextView merchantText = (TextView)merchantView.findViewById(R.id.MerchantName);
            ImageView merchantInfo = (ImageView)merchantView.findViewById(R.id.MerchantInfo);

            merchantText.setText(merchant.MerchantName);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageUrl = voucherHelper.country.equals("NG") ? "http://cms.suregifts.com.ng/temp/" + merchant.ImageName : "http://cms.suregifts.co.ke/temp/" + merchant.ImageName;
            viewHolder.imageView =  merchantImage;
            new DownloadAsyncTask(viewHolder).execute(viewHolder);
            merchantContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voucherHelper.setMerchant_id(merchant.MerchantId);
                    voucherHelper.setMerchant(merchant.MerchantName);
                    voucherHelper.setMerchant_picture(viewHolder.imageUrl);
                    Intent intent = new Intent(SelectMerchant.this, RecipientDetailsActivity.class);
                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                    startActivity(intent);
                }
            });
            merchantInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(SelectMerchant.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.merchantlayoutpoplayout);
                    TextView popupMerchantText = (TextView)dialog.findViewById(R.id.PopUpMerchantName);
                    TextView popupMerchantShortDescription = (TextView)dialog.findViewById(R.id.PopUpMerchantShortDescription);
                    final TextView popupMerchantDescription = (TextView)dialog.findViewById(R.id.PopUpMerchantDescription);
                    ImageView popupImageView = (ImageView)dialog.findViewById(R.id.PopUpMerchantImage);
                    final TextView infotab = (TextView)dialog.findViewById(R.id.InfoTab);
                    final TextView redemptionTab = (TextView)dialog.findViewById(R.id.RedemptionTab);
                    final TextView locationTab = (TextView)dialog.findViewById(R.id.LocationTab);
                    LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
                    popupMerchantText.setText(merchant.MerchantName);
                    popupMerchantDescription.setText(merchant.Description);
                    popupMerchantShortDescription.setText(merchant.ShortDescription);
                    infotab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupMerchantDescription.setText(merchant.Description);
                            infotab.setBackgroundColor(getResources().getColor(R.color.menuColorBackground));
                            infotab.setTextColor(getResources().getColor(R.color.colorWhite));
                            redemptionTab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            redemptionTab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                            locationTab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            locationTab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                        }
                    });
                    redemptionTab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupMerchantDescription.setText(merchant.RedemptionDetails);
                            infotab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            infotab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                            redemptionTab.setBackgroundColor(getResources().getColor(R.color.menuColorBackground));
                            redemptionTab.setTextColor(getResources().getColor(R.color.colorWhite));
                            locationTab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            locationTab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                        }
                    });
                    locationTab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupMerchantDescription.setText(merchant.Locations);
                            infotab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            infotab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                            redemptionTab.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                            redemptionTab.setTextColor(getResources().getColor(R.color.menuColorBackground));
                            locationTab.setBackgroundColor(getResources().getColor(R.color.menuColorBackground));
                            locationTab.setTextColor(getResources().getColor(R.color.colorWhite));
                        }
                    });
                    ViewHolder popupHolder = new ViewHolder();
                    popupHolder.imageUrl = voucherHelper.country.equals("NG") ? "http://cms.suregifts.com.ng/temp/" + merchant.ImageName : "http://cms.suregifts.co.ke/temp/" + merchant.ImageName;
                    popupHolder.imageView = popupImageView;
                    new DownloadAsyncTask(popupHolder).execute(popupHolder);
                    okbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
            container.addView(merchantView);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public class FetchAllMerchants extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            DisplayMerchants();
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                                    .readTimeout(60, TimeUnit.SECONDS)
                                    .writeTimeout(60, TimeUnit.SECONDS)
                                    .connectTimeout(60, TimeUnit.SECONDS)
                                    .build();
            Request request;
            String merchantUrl = voucherHelper.country.equals("NG") ? "http://cms.suregifts.com.ng/api/giftcard/merchantsandcategory" : "http://cms.suregifts.co.ke/api/giftcard/merchantsandcategory";
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();

            Response response;
            try{
                response = client.newCall(request).execute();
                merchantsJson = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        private int position;
        public DownloadAsyncTask(ViewHolder viewHolder)
        {

        }
        @Override
        protected  ViewHolder doInBackground(ViewHolder... params){
            ViewHolder viewHolder = params[0];

            try{
                URL imageUrl = new URL(viewHolder.imageUrl);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageUrl.openStream());
            }catch(IOException e){
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }
        @Override
        protected void onPostExecute(ViewHolder result){
            if (result.bitmap == null){
                result.imageView.setImageResource(android.R.color.transparent);
            }else{
                result.imageView.setImageBitmap(result.bitmap);
            }

        }
    }
    private static class ViewHolder {
        ImageView imageView;
        TextView name;
        String datetime;
        String imageUrl;
        Bitmap bitmap;
        int id;
    }
}
