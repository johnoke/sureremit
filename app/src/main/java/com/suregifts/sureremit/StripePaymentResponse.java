package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/2/2016.
 */
public class StripePaymentResponse {
    public int statusCode;
    public double message;
    public String paymentId;
    public StripePaymentResponse(int statusCode, double message, String paymentId){
        this.statusCode = statusCode;
        this.message =  message;
        this.paymentId = paymentId;
    }
}
