package com.suregifts.sureremit;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SUREGIFTS-DEVPC on 5/19/2016.
 */
public class CategoryAdapter extends BaseAdapter {
    private final Context context;
    public List<Category> categories;
    private ArrayList<Category> categoriesList;

    public CategoryAdapter(Context c, List<Category> categoriesData){
        categories = categoriesData;
        context = c;
        this.categoriesList = new ArrayList<Category>();
        this.categoriesList.addAll(categories);

    }
    @Override
    public int getCount(){
        return categories.size();
    }
    @Override
    public Object getItem(int i){ return categories.get(i);}
    @Override
    public long getItemId(int i){return i;}
    public View getView(int position, View view, ViewGroup parent){
        View rowView = view;
        if (rowView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.category_list, null, true);
        }
        else{
            rowView = view;
        }
        TextView catName = (TextView)rowView.findViewById(R.id.CategoryName);
        LinearLayout layout = (LinearLayout)rowView.findViewById(R.id.CategoryBackground);
        TextView merchantsCount = (TextView)rowView.findViewById(R.id.CategoryMerchantsCount);
        Category category = (Category) categories.get(position);
        catName.setText(category.CategoryName);
        Drawable drawable = context.getResources().getDrawable(category.ImageId);
        layout.setBackground(drawable);
        String noofproducts = "";
        noofproducts = Integer.toString(category.NumberofProducts);
        merchantsCount.setText(noofproducts);
        return rowView;
    }
}
