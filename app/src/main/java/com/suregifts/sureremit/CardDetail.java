package com.suregifts.sureremit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created by SUREGIFTS-DEVPC on 5/26/2016.
 */
public class CardDetail {
    public String CardId;
    public String CardNumber;
    public String CCV;
    public String ExpiryMonth;
    public String ExpiryYear;
    public CardDetail(String cardNumber, String ccv, String expiryMonth, String expiryYear){
        CardNumber = cardNumber;
        CCV = ccv;
        ExpiryMonth = expiryMonth;
        ExpiryYear = expiryYear;
    }
//    public static boolean Add(CardDetail cardDetail, Context context){
//        File file = new File(context.getFilesDir() + "/sureremit_cards_cache.txt");
//        if(file.exists()){
//            try{
//                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_cards_cache.txt")).nextLine();
//                JSONArray jsonArray = new JSONArray(rawdata);
//                List<CardDetail> cards = CardsList(context);
//                cardDetail.CardId = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").toString();
//                cards.add(cardDetail);
//                Gson gson = new GsonBuilder().create();
//                String cardsJSON  = gson.toJson(cards);
//                File cardfile = new File(context.getFilesDir() + "/sureremit_cards_cache.txt");
//                cardfile.delete();
//                String cardJson  = gson.toJson(cards);
//                try {
//                    FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_cards_cache.txt", Context.MODE_WORLD_WRITEABLE);
//                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
//                    outputStreamWriter.write(cardJson);
//                    outputStreamWriter.flush();
//                    outputStreamWriter.close();
//                    return true;
//                }
//                catch (Exception ex)
//                {
//                    ex.printStackTrace();
//                    return false;
//                }
//            }
//            catch(Exception ex){
//                ex.printStackTrace();
//                return false;
//            }
//        }
//        else {
//            List<CardDetail> cards = new ArrayList<CardDetail>();
//            cards.add(cardDetail);
//            Gson gson = new GsonBuilder().create();
//            String cardJSON  = gson.toJson(cards);
//            try {
//                FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_cards_cache.txt", Context.MODE_WORLD_WRITEABLE);
//                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
//                outputStreamWriter.write(cardJSON);
//                outputStreamWriter.flush();
//                outputStreamWriter.close();
//                return true;
//            }
//            catch (Exception ex)
//            {
//                ex.printStackTrace();
//                return false;
//            }
//        }
//    }
//    public static List<Recipient> CardsList(Context context){
//        List<Recipient> recipients = new ArrayList<Recipient>();
//        File file = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
//        if(file.exists()){
//            try{
//                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_recipients_cache.txt")).nextLine();
//                JSONArray jsonArray = new JSONArray(rawdata);
//                for (int i = 0; i < jsonArray.length(); i++)
//                {
//                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                    int recipientId = jsonObject.getInt("RecipientId");
//                    String recipientFullName = jsonObject.getString("RecipientFullName");
//                    String recipientPhone = jsonObject.getString("RecipientPhone");
//                    String recipientEmail = jsonObject.getString("RecipientEmail");
//                    Recipient recipientOne = new Recipient(recipientId, recipientFullName, recipientEmail, recipientPhone);
//                    recipients.add(recipientOne);
//                }
//
//            }
//            catch(Exception ex){
//                ex.printStackTrace();
//
//            }
//        }
//        return recipients;
//    }
    public static boolean Add(CardDetail cardDetail, Context context){
        File file = new File(context.getFilesDir() + "/sureremit_card_details_cache.txt");
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_card_details_cache.txt")).nextLine();
                JSONObject jsonObject = new JSONObject(rawdata);
                Gson gson = new GsonBuilder().create();
                String cardDetailJSON  = gson.toJson(cardDetail);
                File recipientFile = new File(context.getFilesDir() + "/sureremit_card_details_cache.txt");
                recipientFile.delete();
                try {
                    FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_card_details_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(cardDetailJSON);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    return true;
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    return false;
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else {
            Gson gson = new GsonBuilder().create();
            String cardDetailJSON  = gson.toJson(cardDetail);
            try {
                FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_card_details_cache.txt", Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(cardDetailJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();
                return true;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
        }
    }
    public static CardDetail GetCardDetails(Context context){
        CardDetail cardDetail = null;
        File file = new File(context.getFilesDir() + "/sureremit_card_details_cache.txt");
        if (file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_card_details_cache.txt")).nextLine();
                JSONObject jsonObject = new JSONObject(rawdata);
                String cardNumber = jsonObject.getString("CardNumber");
                String ccv = jsonObject.getString("CCV");
                String expiryMonth = jsonObject.getString("ExpiryMonth");
                String expiryYear = jsonObject.getString("ExpiryYear");
                cardDetail = new CardDetail(cardNumber, ccv, expiryMonth, expiryYear);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        return cardDetail;
    }
}
