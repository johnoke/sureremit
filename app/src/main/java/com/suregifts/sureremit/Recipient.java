package com.suregifts.sureremit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by SUREGIFTS-DEVPC on 5/24/2016.
 */
public class Recipient implements Serializable {
    public String RecipientId;
    public String RecipientFullName;
    public String RecipientPhone;
    public String RecipientEmail;
    public Recipient(String recipientId, String recipientFullName, String recipientEmail, String recipientPhone){
        RecipientId = recipientId;
        RecipientFullName = recipientFullName;
        RecipientEmail = recipientEmail;
        RecipientPhone = recipientPhone;
    }
    public static boolean Add(Recipient recipient, Context context){
        File file = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_recipients_cache.txt")).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                List<Recipient> recipients = RecipientsList(context);
                recipient.RecipientId = new SimpleDateFormat("ddMMyyyyhhmmss").toString();
                recipients.add(recipient);
                Gson gson = new GsonBuilder().create();
                String recipientsJSON  = gson.toJson(recipients);
                File recipientFile = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
                recipientFile.delete();
                String recipientJSON  = gson.toJson(recipients);
                try {
                    FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_recipients_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(recipientJSON);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    return true;
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    return false;
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else {
            List<Recipient> recipients = new ArrayList<Recipient>();
            recipients.add(recipient);
            Gson gson = new GsonBuilder().create();
            String recipientJSON  = gson.toJson(recipients);
            try {
                FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_recipients_cache.txt", Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(recipientJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();
                return true;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
        }
    }
    public static List<Recipient> RecipientsList(Context context){
        List<Recipient> recipients = new ArrayList<Recipient>();
        File file = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/sureremit_recipients_cache.txt")).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String recipientId = jsonObject.getString("RecipientId");
                    String recipientFullName = jsonObject.getString("RecipientFullName");
                    String recipientPhone = jsonObject.getString("RecipientPhone");
                    String recipientEmail = jsonObject.getString("RecipientEmail");
                    Recipient recipientOne = new Recipient(recipientId, recipientFullName, recipientEmail, recipientPhone);
                    recipients.add(recipientOne);
                }

            }
            catch(Exception ex){
                ex.printStackTrace();

            }
        }
        return recipients;
    }
    public static void Delete(String recipientId, Context context){
        List<Recipient> Recipients = RecipientsList(context);
        for (Recipient recipient:Recipients) {
            if (recipient.RecipientId.equals(recipientId)){
                Recipients.remove(recipient);
                File recipientFile = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
                if (recipientFile.exists()){
                    recipientFile.delete();
                }
                Gson gson = new GsonBuilder().create();
                String recipientJSON  = gson.toJson(Recipients);
                try {
                    FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_recipients_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(recipientJSON);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();

                }
                break;
            }
        }
    }
    public static void Edit(Recipient editedRecipient, Context context){
        List<Recipient> Recipients = RecipientsList(context);
        for (Recipient recipient:Recipients) {
            if (recipient.RecipientId.equals(editedRecipient.RecipientId)){
                recipient.RecipientPhone = editedRecipient.RecipientPhone;
                recipient.RecipientEmail = editedRecipient.RecipientEmail;
                recipient.RecipientFullName = editedRecipient.RecipientFullName;
                File recipientFile = new File(context.getFilesDir() + "/sureremit_recipients_cache.txt");
                if (recipientFile.exists()){
                    recipientFile.delete();
                }
                Gson gson = new GsonBuilder().create();
                String recipientJSON  = gson.toJson(Recipients);
                try {
                    FileOutputStream fileOutputStream =  context.openFileOutput("sureremit_recipients_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(recipientJSON);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();

                }
                break;
            }
        }
    }
}
