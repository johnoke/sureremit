package com.suregifts.sureremit;

import java.util.List;

/**
 * Created by okeol on 12/8/2017.
 */

public class WalletBalanceResponse {
    public String status;
    public String publicKey;
    public List<WalletBalanceObject> balances;
}
