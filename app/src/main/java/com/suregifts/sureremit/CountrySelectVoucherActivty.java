package com.suregifts.sureremit;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.File;

public class CountrySelectVoucherActivty extends AppCompatActivity {
    Toolbar countrySelectVoucherToolbar;
    TextView voucherCountrySelectText, nigeriaText, kenyaText, selectVoucherTitleText;
    private DrawerLayout menuDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_select_voucher_activty);
        setTitle("");
        countrySelectVoucherToolbar = (Toolbar)findViewById(R.id.CountrySelectVoucherToolbar);
        setSupportActionBar(countrySelectVoucherToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        voucherCountrySelectText = (TextView) findViewById(R.id.VoucherSelectCountryText);
        Typeface font = Typeface.createFromAsset(getAssets(), "Lato-BoldItalic.ttf");
        Typeface fontRegular = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        Typeface fontBold = Typeface.createFromAsset(getAssets(), "Lato-Bold.ttf");
        nigeriaText = (TextView)findViewById(R.id.SendAirtimeNigeriaText);
        kenyaText = (TextView)findViewById(R.id.SendAirtimeKenyaText);
        selectVoucherTitleText = (TextView)findViewById(R.id.SendVoucherTitleText);
        voucherCountrySelectText.setText("Select the country you are\nsending a voucher to ");
        nigeriaText = (TextView)findViewById(R.id.SelectVoucherNigeriaText);
        kenyaText = (TextView)findViewById(R.id.SelectVoucherKenyaText);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            switch(item.getItemId()) {
                case R.id.action_drawer:
                    menuDrawer.openDrawer(GravityCompat.START);
                    break;
                case R.id.action_history:
                    Intent intent = new Intent(this, OrderHistoryActivity.class);
                    startActivity(intent);
                    break;
                default:
                    Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                    finish();
                    return true;
            }
        return super.onOptionsItemSelected(item);
    }
    public void LaunchCategorySendVoucher(View view)
    {
        Intent intent = new Intent(this, ListCategoriesActivity.class);
        String country = "NG";
        intent.putExtra("country", country);
        startActivity(intent);
    }
    public void LaunchCategorySendVoucherKE(View view)
    {
        Intent intent = new Intent(this, ListCategoriesActivity.class);
        String country = "KE";
        intent.putExtra("country", country);
        startActivity(intent);
    }
}
