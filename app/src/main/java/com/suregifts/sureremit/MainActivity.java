package com.suregifts.sureremit;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.io.File;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, CacheDataService.class));
//        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        File walkThrough = new File(getFilesDir() + "/sureremit_walkthrough_cache.txt");
        NotificationCompat.Builder mBuilder = new  NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.sureremiticon);
        mBuilder.setContentTitle("SureRemit Successfully Installed");
        mBuilder.setContentText("Hi, Welcome to SureRemit");
//        Intent resultIntent = new Intent(this, MainActivity.class);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(MainActivity.class);

        auth = FirebaseAuth.getInstance();
        if (walkThrough.exists()) {
            if (auth.getCurrentUser() == null) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            } else {
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        }
        else{
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(1, mBuilder.build());
            Intent intent = new Intent(this, WalkThroughActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public UserProfile GetUserData()
    {
        UserProfile userProfile = new UserProfile();
        try{
            String rawdata = new Scanner(new File(getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            Log.e("Login Response", rawdata);
            userProfile.Email = obj.getString("Email");
            userProfile.FirstName = obj.getString("FirstName");
            userProfile.LastName = obj.getString("LastName");
            userProfile.PhoneNumber = obj.getString("PhoneNumber");
            userProfile.Country = obj.getString("Country");
            userProfile.City = obj.getString("City");
            userProfile.ZipCode = obj.getString("ZipCode");
            userProfile.Address = obj.getString("Address");
            Log.e("Login Response", rawdata);

        }
        catch (Exception ex){

        }
        return userProfile;
    }
}
