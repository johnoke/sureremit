package com.suregifts.sureremit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class EditProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private EditText email, firstname, lastname, phonenumber;
    private TextInputLayout emailLayout, firstnameLayout, lastnameLayout, phonenumberLayout;
    private AppCompatButton submitButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    UserProfile userProfile;
    ProgressDialog progressDialog;
    UserHelper userHelper;
    public EditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        email = (EditText)v.findViewById(R.id.ProfileEmail);
        firstname = (EditText)v.findViewById(R.id.ProfileFirstName);
        lastname = (EditText)v.findViewById(R.id.ProfileLastName);
        phonenumber = (EditText)v.findViewById(R.id.ProfilePhone);
        emailLayout = (TextInputLayout)v.findViewById(R.id.ProfileEmailLayout);
        firstnameLayout = (TextInputLayout)v.findViewById(R.id.ProfileFirstNameLayout);
        submitButton = (AppCompatButton)v.findViewById(R.id.EditProfileButton);
        lastnameLayout = (TextInputLayout)v.findViewById(R.id.ProfileLastNameLayout);
        phonenumberLayout = (TextInputLayout)v.findViewById(R.id.ProfilePhoneLayout);
        progressDialog = new ProgressDialog(getActivity());
        email.addTextChangedListener(new MyTextWatcher(email));
        firstname.addTextChangedListener(new MyTextWatcher(firstname));
        lastname.addTextChangedListener(new MyTextWatcher(lastname));

        phonenumber.addTextChangedListener(new MyTextWatcher(phonenumber));
        userProfile = new UserProfile("","","","","");
        GetUserData();
        email.setText(userProfile.Email);
        firstname.setText(userProfile.FirstName);
        lastname.setText(userProfile.LastName);
        phonenumber.setText(userProfile.PhoneNumber);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validateFirstName() && validateLastName()){
                    userHelper = new UserHelper();
                    userHelper.setFirstname(firstname.getText().toString());
                    userHelper.setLastname(lastname.getText().toString());
                    userHelper.setEmail(email.getText().toString());
                    userHelper.setPhone(phonenumber.getText().toString());
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Saving Changes");
                    progressDialog.show();
                    new AsyncProfileUpdate().execute();
                }
            }
        });
        return v;
    }
    private boolean validateEmail(){
        String strEmail = email.getText().toString();
        if (strEmail.equals("") || strEmail.equals(null)) {
            emailLayout.setError("Email is required");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateFirstName(){
        String strFirstName = firstname.getText().toString();
        if (strFirstName.equals("") || strFirstName.equals(null)) {
            firstnameLayout.setError("First Name is required");
            firstnameLayout.requestFocus();
            firstnameLayout .setErrorEnabled(true);
            return false;
        } else {
            firstnameLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateLastName(){
        String strLastName = lastname.getText().toString();
        if (strLastName.equals("") || strLastName.equals(null)) {
            lastnameLayout.setError("Last Name is required");
            lastnameLayout.requestFocus();
            lastnameLayout.setErrorEnabled(true);
            return false;
        } else {
            lastnameLayout.setErrorEnabled(false);
        }

        return true;
    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.ProfileEmail:
                    validateEmail();
                    break;

                case R.id.ProfileFirstName:
                    validateFirstName();
                    break;
                case R.id.ProfileLastName:
                    validateLastName();
                    break;

            }
        }
    }
    public void GetUserData()
    {
        try{
            String rawdata = new Scanner(new File(getContext().getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            userProfile.Email = obj.getString("Email");
            userProfile.FirstName = obj.getString("FirstName");
            userProfile.LastName = obj.getString("LastName");
            userProfile.PhoneNumber = obj.getString("PhoneNumber");
        }
        catch (Exception ex){

        }

    }
    public class AsyncProfileUpdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String result;
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(userHelper);
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/profile/update")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
            }
            catch(IOException ex)
            {
                result = "Server Error";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject obj = new JSONObject(s);
                String status = obj.getString("status");
                if(status.equals("0")){
                    String phone = "";
                    UserProfile userProfile = new UserProfile(userHelper.firstname, userHelper.lastname, userHelper.email, "", userHelper.phone);
                    Gson gson = new GsonBuilder().create();
                    String userData = gson.toJson(userProfile);
                    try {
                       FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                       OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                       outputStreamWriter.write(userData);
                       outputStreamWriter.flush();
                       outputStreamWriter.close();
                       DisplaySuccessPopUp("Profile Saved Successfully", "Changes made to your profile was saved successfully");
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
                    }
                } else {
                    DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
                }
            } catch (Exception ex) {
                DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
            }

            progressDialog.dismiss();

        }
    }
    public void DisplayErrorPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void DisplaySuccessPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        TextView dismissButton = (TextView)dialog.findViewById(R.id.PopUpDismissButton);
        dismissButton.setText("Back to Main Menu");
        popupImage.setImageResource(R.drawable.check);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
