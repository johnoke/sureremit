package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 5/11/2016.
 */
public class UserProfile {
    public String FirstName;
    public String LastName;
    public String Email;
    public String Password;
    public String PhoneNumber;
    public String Country;
    public String ZipCode;
    public String City;
    public String Address;
    public UserProfile(){

    }
    public UserProfile(String firstName, String lastName, String email, String password, String phoneNumber)
    {
         this.FirstName = firstName;
         this.LastName = lastName;
         this.Email = email;
         this.Password = password;
         this.PhoneNumber = phoneNumber;
    }
}
