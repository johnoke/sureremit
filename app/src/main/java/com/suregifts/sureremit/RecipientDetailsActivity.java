package com.suregifts.sureremit;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class RecipientDetailsActivity extends AppCompatActivity {
    private DrawerLayout menuDrawer;
    Toolbar toolbar;
    EditText amountNaira, packageLabel;
    TextInputLayout amountNairaLayout, packageLabelLayout;
    TextView amountLocalCurrencyLabel;
    FontEditText amountDollars;
    TextInputLayout recipientEmailLayout;
    TextInputLayout recipientPhoneLayout;
    TextInputLayout recipientFullNameLayout;
    FontEditText recipientEmail;
    FontEditText recipientPhone;
    FontEditText recipientFullName;

    LinearLayout recipientslistContainer;
    FontButton addRecipientButton;
    TextView toolBarTitle;
    TextView enterRecipientLabel;
    TextView spinnerLabel;
    Spinner packagesSpinner;
    TextView valueLabel;
    FontButton proceedButton;
    ImageView stepperImage;
    double dollarToNaira = 263;
    double dollarToKsh = 101.35;
    double dollarToFrw = 779;
    VoucherHelper voucherHelper;
    List<Recipient> recipients;
    LinearLayout emptyContainer;
    List<CheckBox> checkboxes;
    String[] packagesStrings;
    double[] packagesDoubles;
    int[] packagesIds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        voucherHelper = (VoucherHelper) intent.getSerializableExtra("voucherHelper");
        if (voucherHelper.optype.equals("Voucher")){
            setTheme(R.style.VoucherTheme);
        }
        else if(voucherHelper.optype.equals("Airtime")){
            setTheme(R.style.Airtime);
        }
        else{
            setTheme(R.style.PayBills);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipient_details);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
        amountNaira = (EditText)findViewById(R.id.VoucherAmountNaira);
        recipientEmailLayout = (TextInputLayout)findViewById(R.id.RecipientEmailLayout);
        recipientPhoneLayout = (TextInputLayout)findViewById(R.id.RecipientPhoneLayout);
        recipientFullNameLayout = (TextInputLayout)findViewById(R.id.RecipientFullNameLayout);
        recipientEmail = (FontEditText)findViewById(R.id.RecipientEmail);
        recipientPhone = (FontEditText)findViewById(R.id.RecipientPhone);
        recipientFullName = (FontEditText)findViewById(R.id.RecipientFullName);
        amountNairaLayout = (TextInputLayout)findViewById(R.    id.VoucherAmountNairaLayout);
        packageLabelLayout = (TextInputLayout)findViewById(R.id.PackageLabelLayout);
        amountDollars = (FontEditText)findViewById(R.id.VoucherAmountDollars);
        amountNaira.addTextChangedListener(new MyTextWatcher(amountNaira));
        recipientEmail.addTextChangedListener(new MyTextWatcher(recipientEmail));
        recipientPhone.addTextChangedListener(new MyTextWatcher(recipientPhone));
        recipientFullName.addTextChangedListener(new MyTextWatcher(recipientFullName));

        addRecipientButton = (FontButton)findViewById(R.id.AddRecipientButton);
        packagesSpinner = (Spinner)findViewById(R.id.PackagesSpinner);
        addRecipientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchAddRecipient(v);
            }
        });
        recipients = Recipient.RecipientsList(RecipientDetailsActivity.this);
        emptyContainer = (LinearLayout)findViewById(R.id.EmptyRecipients);
        recipientslistContainer = (LinearLayout)findViewById(R.id.RecipientsList);
        proceedButton = (FontButton)findViewById(R.id.ProceedButton);

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePackageLabel())
                ProceedToPay();
            }
        });
        spinnerLabel = (TextView)findViewById(R.id.SpinnerLabel);
        packageLabel = (EditText)findViewById(R.id.PackageLabel);
        checkboxes = new ArrayList<CheckBox>();
//        if (recipients.size() > 0){
//            emptyContainer.setVisibility(View.GONE);
//            DisplayRecipients();
//        }
//        else{
//            emptyContainer.setVisibility(View.VISIBLE);
//        }

        setTitle("");
        toolBarTitle = (TextView)findViewById(R.id.SendVoucherTitleText);
        valueLabel = (TextView)findViewById(R.id.VoucherValueLabel);
        enterRecipientLabel = (TextView)findViewById(R.id.EnterRecipientLabel);
        stepperImage = (ImageView)findViewById(R.id.StepperImage);
        if (voucherHelper.optype.equals("Voucher")){
            toolBarTitle.setText("Recipient Details");
            stepperImage.setImageResource(R.drawable.voucherstepperthree);
            valueLabel.setText("Voucher Value");
            enterRecipientLabel.setText("Enter Voucher's Recipient Details");
        }
        else if(voucherHelper.optype.equals("Airtime")){
            toolBarTitle.setText("Recipient Details");
            stepperImage.setImageResource(R.drawable.airtimesteppertwo);
            valueLabel.setText("Airtime Value");
            enterRecipientLabel.setText("Enter Airtime's Recipient Details");

        }
        else{
            toolBarTitle.setText("Recipient Details");
            stepperImage.setImageResource(R.drawable.billstepperthree);
            valueLabel.setText("Bill Value");
            enterRecipientLabel.setText("Enter Bill's Recipient Details");
            packageLabelLayout.setVisibility(View.VISIBLE);
            packageLabel.setHint(voucherHelper.getMerchant_label());
        }
        if (voucherHelper.country.equals("NG")) {
            amountNaira.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.naira, 0);
        }
        else if(voucherHelper.country.equals("KE")){
            amountNaira.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.ksh, 0);
        }
        else{
            amountNaira.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.frw, 0);
        }
        if (voucherHelper.amount_payable != null){
            amountDollars.setText(voucherHelper.amount_payable);
        }
        if (voucherHelper.amount != null){
            amountNaira.setText(voucherHelper.amount);
        }
        if (voucherHelper.recipient_email != null){
            recipientEmail.setText(voucherHelper.recipient_email);
        }
        if (voucherHelper.recipient_name != null){
            recipientFullName.setText(voucherHelper.recipient_name);
        }
        if (voucherHelper.recipient_phone != null){
            recipientPhone.setText(voucherHelper.recipient_phone);
        }
        if (voucherHelper.BillPackages != null && voucherHelper.BillPackages.size() > 0){
            spinnerLabel.setVisibility(View.VISIBLE);
            packagesSpinner.setVisibility(View.VISIBLE);
            packagesStrings = new String[voucherHelper.BillPackages.size()];
            packagesDoubles = new double[voucherHelper.BillPackages.size()];
            packagesIds = new int[voucherHelper.BillPackages.size()];
            for (int i = 0; i < packagesStrings.length; i++){
                packagesStrings[i] = voucherHelper.BillPackages.get(i).Label;
                packagesDoubles[i] = voucherHelper.BillPackages.get(i).Amount;
                packagesIds[i] = voucherHelper.BillPackages.get(i).PackageId;
            }
            ArrayAdapter<String> packagesAdapter = new ArrayAdapter<String>(this, R.layout.spinnerrow, R.id.PackageName, packagesStrings);
            packagesSpinner.setAdapter(packagesAdapter);
            packagesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Double doubleAmountnaira = packagesDoubles[position];
                    double dollarequivalent = 0.0;
                    if (voucherHelper.country.equals("NG")) {
                        voucherHelper.setMerchant_id(packagesIds[position]);
                        dollarequivalent = doubleAmountnaira / dollarToNaira;
                        dollarequivalent = round(dollarequivalent, 2);
                        voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                        voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                    }
                    else if (voucherHelper.country.equals("KE")) {
                        dollarequivalent = doubleAmountnaira / dollarToKsh;
                        dollarequivalent = round(dollarequivalent, 2);
                        voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                        voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                    }
                    else {
                        dollarequivalent = doubleAmountnaira / dollarToFrw;
                        dollarequivalent = round(dollarequivalent, 2);
                        voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                        voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                    }
                    amountDollars.setText(Double.toString(dollarequivalent));
                    amountNaira.setText(Double.toString(doubleAmountnaira));
                    if (voucherHelper.getRecipient_email() != "" && voucherHelper.getRecipient_email() != null &&
                            voucherHelper.getRecipient_name() != "" && voucherHelper.getRecipient_name() != null &&
                            voucherHelper.getRecipient_phone() != "" && voucherHelper.getRecipient_phone() != null) {
                        proceedButton.setAlpha(1);
                        proceedButton.setFocusable(true);
                        proceedButton.setEnabled(true);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.VoucherAmountNaira:
                    validateAmount();
                    break;
                case R.id.RecipientEmail:
                    validateEmail();
                    break;
                case R.id.RecipientFullName:
                    validateFullName();
                    break;
                case R.id.RecipientPhone:
                    validatePhoneNumber();
                    break;
            }
        }
    }
    private boolean validatePackageLabel(){
        String strPackageLabel = packageLabel.getText().toString();
        if (strPackageLabel != null && strPackageLabel != ""){
            voucherHelper.setPackage_number(strPackageLabel);
            packageLabelLayout.setError(null);
            return true;
        }
        else{
            packageLabelLayout.setError("This field is required");
            packageLabelLayout.requestFocus();
            packageLabelLayout.setErrorEnabled(true);
            return false;
        }
    }
    private boolean validateAmount(){
        String strAmountNaira = amountNaira.getText().toString();
        try {
            Double doubleAmountnaira = Double.parseDouble(strAmountNaira);
            double dollarequivalent = 0.0;
            if (voucherHelper.country.equals("NG")) {
                dollarequivalent = doubleAmountnaira / dollarToNaira;
                dollarequivalent = round(dollarequivalent, 2);
            }else if(voucherHelper.country.equals("KE")){
                dollarequivalent = doubleAmountnaira / dollarToKsh;
                dollarequivalent = round(dollarequivalent, 2);
            }else{
                dollarequivalent = doubleAmountnaira / dollarToFrw;
                dollarequivalent = round(dollarequivalent, 2);
            }
            if (doubleAmountnaira < 200) {
                amountNairaLayout.setError("Value must be at least 200");
                amountNairaLayout.requestFocus();
                amountNairaLayout.setErrorEnabled(true);
                amountDollars.setText("0.0");
                return false;
            } else if(dollarequivalent > 2000){
                amountNairaLayout.setError("Maximum Transaction limit reached");
                amountNairaLayout.requestFocus();
                amountNairaLayout.setErrorEnabled(true);
                amountDollars.setText("0.0");
                return false;
            }else {
                amountNairaLayout.setError(null);
                if (voucherHelper.country.equals("NG"))
                {
                    voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                    voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                }
                else if (voucherHelper.country.equals("KE"))
                {
                    voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                    voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                }
                else {
                    voucherHelper.setAmount(String.valueOf(doubleAmountnaira));
                    voucherHelper.setAmount_payable(String.valueOf(dollarequivalent));
                }
                amountDollars.setText(Double.toString(dollarequivalent));

                return true;
            }
        }
        catch(Exception ex) {
            amountNairaLayout.setError("Voucher Value must be at least 200");
            amountNairaLayout.requestFocus();
            amountNairaLayout.setErrorEnabled(true);

            return false;
        }
    }
    private boolean validateFullName(){
        String strFullName = recipientFullName.getText().toString();
        if (strFullName.equals("") || strFullName.equals(null)) {
            recipientFullNameLayout.setError("Full Name is required");
            recipientFullNameLayout.requestFocus();
            recipientFullNameLayout .setErrorEnabled(true);
            return false;
        } else {
            recipientFullNameLayout.setError(null);
            voucherHelper.setRecipient_name(strFullName);
        }

        return true;
    }
    private boolean validateEmail(){
        String strEmail = recipientEmail.getText().toString();
        strEmail = strEmail.trim();
        String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        boolean validateEmail = strEmail.matches(EMAIL_REGEX);
        if (strEmail.equals("") || strEmail.equals(null)) {
            recipientEmailLayout.setError("Email is required");
            recipientEmailLayout.requestFocus();
            recipientEmailLayout .setErrorEnabled(true);
            return false;
        }else if(!validateEmail) {
            recipientEmailLayout.setError("Email is not in the right format");
            recipientEmailLayout.requestFocus();
            recipientEmailLayout.setErrorEnabled(true);
            return false;
        }else {
            voucherHelper.setRecipient_email(strEmail);
            recipientEmailLayout.setError(null);
        }

        return true;
    }
    private boolean validatePhoneNumber(){
        String strPhone = recipientPhone.getText().toString();
        if (strPhone.equals("") || strPhone.equals(null)) {
            recipientPhoneLayout.setError("Phone Number is required");
            recipientPhoneLayout.requestFocus();
            recipientPhoneLayout .setErrorEnabled(true);
            return false;
        } else {
            recipientPhoneLayout.setError(null);
            voucherHelper.setRecipient_phone(strPhone);
        }

        return true;
    }
    public void ProceedToPay(){
        if(validateAmount() && validateEmail() && validatePhoneNumber() && validateFullName() && validatePackageLabel()){
            Intent intent = new Intent(this, OrderSummaryActivity.class);
            intent.putExtra("voucherHelper", (Serializable) voucherHelper);
            startActivity(intent);
        }

    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    public void LaunchAddRecipient(View view){
        Intent intent = new Intent(this, AddRecipientActivity.class);
        intent.putExtra("voucherHelper", (Serializable) voucherHelper);
        startActivity(intent);
    }
    public void RemoveRecipients(){
        recipientslistContainer.removeAllViews();
        recipients = Recipient.RecipientsList(RecipientDetailsActivity.this);
        if (recipients.size() > 0){
            emptyContainer.setVisibility(View.GONE);
            DisplayRecipients();
        }
        else{
            emptyContainer.setVisibility(View.VISIBLE);
        }
    }
    public void DisplayRecipients(){
        for(final Recipient recipientItem:recipients){
            try{
                LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View recipientView = layoutInflater.inflate(R.layout.recipientcard, null);
                CardView recipientContainer = (CardView)recipientView.findViewById(R.id.RecipientContainer);
                ImageView trashIcon = (ImageView)recipientView.findViewById(R.id.TrashIcon);
                trashIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Recipient.Delete(recipientItem.RecipientId, getApplicationContext());
                        RemoveRecipients();
                    }
                });
                ImageView editIcon = (ImageView)recipientView.findViewById(R.id.EditIcon);
                editIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), EditRecipientActivity.class);
                        intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                        intent.putExtra("recipient", (Serializable) recipientItem);
                        startActivity(intent);
                        finish();
                    }
                });
                final CheckBox checkBox = (CheckBox)recipientView.findViewById(R.id.RecipientCheckBox);
                TextView recipientName = (TextView)recipientView.findViewById(R.id.RecipientFullName);
                TextView recipientPhone = (TextView)recipientView.findViewById(R.id.RecipientPhone);
                recipientName.setText(recipientItem.RecipientFullName);
                recipientPhone.setText(recipientItem.RecipientPhone);
                recipientslistContainer.addView(recipientView);
                checkboxes.add(checkBox);
                checkBox.setOnCheckedChangeListener(
                        new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked){
                                    for(final CheckBox checkBox2:checkboxes){
                                        checkBox2.setChecked(false);
                                    }
                                    checkBox.setChecked(true);
                                    voucherHelper.setRecipient_email(recipientItem.RecipientEmail);
                                    voucherHelper.setRecipient_name(recipientItem.RecipientFullName);
                                    voucherHelper.setRecipient_phone(recipientItem.RecipientPhone);
                                    if(validateAmount()){
                                        proceedButton.setAlpha(1);
                                        proceedButton.setFocusable(true);
                                        proceedButton.setEnabled(true);
                                    }
                                }
                                else{
                                    proceedButton.setAlpha(0.4f);
                                    proceedButton.setFocusable(false);
                                    proceedButton.setEnabled(false);
                                }
                            }
                        }
                );
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

        }

    }
}
