package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/6/2016.
 */
public class UserHelper {
    public String token;
    public String uid;
    public String email;
    public String firstname;
    public String lastname;
    public String phone;
    public String status;
    public String country;
    public String city;
    public String zipcode;
    public String address;
    public UserHelper(){

    }
    public UserHelper(String uid, String email, String firstname, String lastname){
        this.uid = uid;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
    }
    public String getEmail(){ return this.email; }
    public void setEmail(String email){this.email = email; }
    public String getFirstname(){ return this.firstname; }
    public void setFirstname(String firstname){this.firstname = firstname; }
    public String getLastname(){ return this.lastname; }
    public void setLastname(String lastname){this.lastname = lastname; }
    public String getPhone(){ return this.phone; }
    public void setPhone(String phone){this.phone = phone; }
    public String getCountry(){ return this.country; }
    public void setCountry(String country){this.country = country; }
    public String getCity(){ return this.city; }
    public void setCity(String city){this.city = city; }
    public String getZipcode(){ return this.zipcode; }
    public void setZipcode(String zipcode){this.zipcode = zipcode; }
    public String getAddress(){ return this.address; }
    public void setAddress(String address){this.address = address; }
    public static boolean ProfileCompleted(UserProfile profile){
        if (IsNullOrEmpty(profile.FirstName) || IsNullOrEmpty(profile.LastName) || IsNullOrEmpty(profile.Email) &&
            IsNullOrEmpty(profile.PhoneNumber) || IsNullOrEmpty(profile.Country) || IsNullOrEmpty(profile.City)){
            return false;
        }
        else{
            return true;
        }
    }
    public static boolean IsNullOrEmpty(String value){
        if (value == null || value.isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
}
