package com.suregifts.sureremit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;


public class RecipientsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    LinearLayout recipientslistContainer;
    List<Recipient> recipients;
    FontButton addRecipientButton;
    LinearLayout emptyContainer;
    // TODO: Rename and change types of parameters




    public RecipientsFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_recipients, container, false);
        recipientslistContainer = (LinearLayout)v.findViewById(R.id.RecipientsList);
        emptyContainer = (LinearLayout)v.findViewById(R.id.EmptyRecipients);
        recipients = Recipient.RecipientsList(getContext());
        if (recipients.size() > 0){
            emptyContainer.setVisibility(View.GONE);
            DisplayRecipients();
        }
        else{
            emptyContainer.setVisibility(View.VISIBLE);
        }
        addRecipientButton = (FontButton)v.findViewById(R.id.AddRecipientButton);
        addRecipientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchAddRecipient(v);
            }
        });
        return v;
    }
    public void DisplayRecipients(){
        for(final Recipient recipientItem:recipients){
            try{
                LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View recipientView = layoutInflater.inflate(R.layout.profilerecipientcard, null);
                LinearLayout recipientContainer = (LinearLayout)recipientView.findViewById(R.id.RecipientContainer);
                ImageView trashIcon = (ImageView)recipientView.findViewById(R.id.TrashIcon);
                trashIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Recipient.Delete(recipientItem.RecipientId,getActivity().getApplicationContext());
                        RemoveRecipients();
                    }
                });
                ImageView editIcon = (ImageView)recipientView.findViewById(R.id.EditIcon);
                editIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity().getApplicationContext(), EditRecipientActivity.class);
                        intent.putExtra("recipient", (Serializable) recipientItem);
                        startActivity(intent);
                        getActivity().finish();
                    }
                });
                TextView recipientName = (TextView)recipientView.findViewById(R.id.RecipientFullName);
                TextView recipientPhone = (TextView)recipientView.findViewById(R.id.RecipientPhone);
                recipientName.setText(recipientItem.RecipientFullName);
                recipientPhone.setText(recipientItem.RecipientPhone);
                recipientslistContainer.addView(recipientView);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

        }
    }
    public void RemoveRecipients(){
        recipientslistContainer.removeAllViews();
        recipients = Recipient.RecipientsList(getActivity().getApplicationContext());
        if (recipients.size() > 0){
            emptyContainer.setVisibility(View.GONE);
            DisplayRecipients();
        }
        else{
            emptyContainer.setVisibility(View.VISIBLE);
        }
    }
    public void LaunchAddRecipient(View view){
        Intent intent = new Intent(getContext(), AddRecipientActivity.class);
        startActivity(intent);
    }

    // TODO: Rename method, update argument and hook method into UI event



}
