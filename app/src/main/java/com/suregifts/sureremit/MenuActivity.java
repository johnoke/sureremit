package com.suregifts.sureremit;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class MenuActivity extends AppCompatActivity {
    Toolbar menuToolbar;
    FontTextView voucherText, payBillsText, sendAirtimeText;
    FontBoldItalicTextView welcomeText;
    ImageView voucherImage, payBillsImage;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout menuDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        menuToolbar = (Toolbar)findViewById(R.id.MenuToolBar);
        int remitLogo = getResources().getIdentifier("barlogo", "drawable", "com.suregifts.sureremit");
        menuToolbar.setLogo(remitLogo);
        setSupportActionBar(menuToolbar);
        welcomeText = (FontBoldItalicTextView)findViewById(R.id.MenuWelcomeText);
        payBillsText= (FontTextView)findViewById(R.id.PayBillsText);
        voucherText = (FontTextView)findViewById(R.id.VoucherText);
        voucherImage = (ImageView)findViewById(R.id.VoucherImage);
        sendAirtimeText = (FontTextView)findViewById(R.id.SendAirtimeText);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);

        payBillsImage = (ImageView)findViewById(R.id.PayBillsImage);
        String userFirstName = "";
        try{
            String rawdata = new Scanner(new File(getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            userFirstName = obj.getString("FirstName");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        welcomeText.setText("Hello "+ userFirstName +"!");
        setTitle("");
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.suregifts.sureremit",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finishActivity(1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void LaunchVoucherCountrySelect(View view)
    {
        Intent intent = new Intent(this, CountrySelectVoucherActivty.class);
        startActivity(intent);
    }
    public void LaunchPayBills(View view)
    {
        Intent intent = new Intent(this, CountrySelectPayBillsActivity.class);
        startActivity(intent);
    }
    public void LaunchAirtime(View view)
    {
        Intent intent = new Intent(this, CountrySelectSendAirtimeActivity.class);
        startActivity(intent);
    }
    public void CloseNavigationDrawer(View view){
        try {

           menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public void Logout(View view){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null){
            auth.signOut();
//            File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
//            file.delete();
            Intent intent = new Intent(this, LoginActivity.class);
            ComponentName cn = intent.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(cn);
            startActivity(mainIntent);
        }

    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Wallet(View view){
        Intent intent = new Intent(this, WalletActivity.class);
        startActivity(intent);
    }
}
