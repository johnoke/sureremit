package com.suregifts.sureremit;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stripe.android.model.Card;

import java.io.Serializable;

public class EditCardActivity extends AppCompatActivity {
    Toolbar toolbar;
    VoucherHelper voucherHelper;
    MaskedFontText cardNumber, expiryDate, ccv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable upArrow = getResources().getDrawable(R.drawable.close);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        cardNumber = (MaskedFontText)findViewById(R.id.CardNumber);
        expiryDate = (MaskedFontText)findViewById(R.id.CardExpiryDate);
        ccv = (MaskedFontText)findViewById(R.id.CardCCV);
        Intent intent = getIntent();
        voucherHelper = (VoucherHelper) intent.getSerializableExtra("voucherHelper");
        setTitle("");
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recipientoptions, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_save_recipient:
                SaveCard();
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void SaveCard(){
        String strCardNumber = cardNumber.getUnmaskedText().toString();
        String strCCV = ccv.getUnmaskedText().toString();
        String strExpiryDate = expiryDate.getText().toString();
        String[] expirymonthyear = strExpiryDate.split("/");
        int month;
        int year;
        month = Integer.parseInt(expirymonthyear[0].replaceAll(" ", ""));
        year = Integer.parseInt(expirymonthyear[1].replaceAll(" ", ""));
        Card card = new Card(strCardNumber, month, year, strCCV);
        if (!card.validateCard()){
            DisplayErrorPopUp("Invalid Card Details", "The Card Details you provided is incorrect, Please Check and Try again");
        }
        else{
            if(voucherHelper != null){
                CardDetail.Add(new CardDetail(strCardNumber, strCCV, expirymonthyear[0], expirymonthyear[1]), this);
                Intent intent = new Intent(this, OrderSummaryActivity.class);
                intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                startActivity(intent);
                finish();
            }
            else{
                finish();
            }
        }
    }
    public void DisplayErrorPopUp(String Title, String Message){
        final Dialog dialog = new Dialog(EditCardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
