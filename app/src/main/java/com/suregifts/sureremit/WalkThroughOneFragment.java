package com.suregifts.sureremit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WalkThroughOneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WalkThroughOneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkThroughOneFragment extends Fragment {

    private FontButton nextButton;
    public WalkThroughOneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WalkThroughOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WalkThroughOneFragment newInstance(String param1, String param2) {
        WalkThroughOneFragment fragment = new WalkThroughOneFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_walk_through_one, container, false);
        nextButton = (FontButton)v.findViewById(R.id.FragmentOneNextButton);
        try {
            FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_walkthrough_cache.txt", Context.MODE_WORLD_WRITEABLE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            outputStreamWriter.write("Walked Through");
            outputStreamWriter.flush();
            outputStreamWriter.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();

        }
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((WalkThroughActivity)getActivity()).setCurrentItem(1, true);
            }
        });
        FontButton leaveButton = (FontButton)v.findViewById(R.id.LeaveWalkthroughButton);
        leaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return v;
    }


}
