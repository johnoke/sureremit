package com.suregifts.sureremit;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BillsCategoryActivity extends AppCompatActivity {
    private DrawerLayout menuDrawer;
    String country = "";
    Toolbar listCategoriesToolbar;
    List<BillCategory> categoriesList;
    LinearLayout categoriesContainer;
    ProgressDialog progressDialog;
    String categoriesJSON = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_category);
        listCategoriesToolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(listCategoriesToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
        Intent intent = getIntent();
        categoriesList = new ArrayList<BillCategory>();
        categoriesContainer = (LinearLayout)findViewById(R.id.CategoriesContainer);
        country = intent.getStringExtra("country");
        File file = new File(getFilesDir() + "/sureremit_bills_cache_" + country +".txt");
        if(file.exists()){
            try{
                categoriesJSON = new Scanner(new File(getFilesDir() + "/sureremit_bills_cache_" + country +".txt")).nextLine();
                LoadCategory();
            }catch(Exception ex){
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please Wait...");
                progressDialog.show();
                new FetchAllCategories().execute();
            }
        }else{
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please Wait...");
            progressDialog.show();
            new FetchAllCategories().execute();
        }
        setTitle("");
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    public void LoadCategory(){
        Gson gson = new GsonBuilder().create();
        try{
            JSONArray jsonArray = new JSONArray(categoriesJSON);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String billsStr = jsonObject.toString();
                BillCategory billCategory = gson.fromJson(billsStr, BillCategory.class);
                billCategory.Pictureurl = billCategory.Pictureurl.substring(0, billCategory.Pictureurl.length() - 4);
                billCategory.PictureId = getResources().getIdentifier(billCategory.Pictureurl, "drawable", "com.suregifts.sureremit");
                categoriesList.add(billCategory);
            }
            BindCategories();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void BindCategories() {
        for (final BillCategory category : categoriesList) {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View categoryView = layoutInflater.inflate(R.layout.category_list, null);
            LinearLayout catLayout = (LinearLayout) categoryView.findViewById(R.id.CategoryBackground);
            TextView merchantsCount = (TextView) categoryView.findViewById(R.id.CategoryMerchantsCount);
            TextView categoryText = (TextView) categoryView.findViewById(R.id.CategoryName);
            categoryText.setText(category.CategoryName);
            merchantsCount.setText(Integer.toString(category.Count));

            try{
                Drawable drawable = getResources().getDrawable(category.PictureId);
                catLayout.setBackground(drawable);
            }catch(Exception ex){
                ex.printStackTrace();
            }
            catLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoadBills(category);
                }
            });
            categoriesContainer.addView(categoryView);
        }
    }
    public class FetchAllCategories extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            try {
                FileOutputStream fileOutputStream =  getApplicationContext().openFileOutput("sureremit_bills_cache_" + country +".txt", Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(categoriesJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            LoadCategory();
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String merchantUrl = "";
            if(country.equals("NG")) {
                merchantUrl = "http://cms.suregifts.com.ng/api/utilities";
            }else if(country.equals("KE")){
                merchantUrl = "http://cms.suregifts.co.ke/api/utilities";
            } else{
                merchantUrl = "http://cms.suregifts.rw/api/utilities";
            }
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();

            Response response;
            try{
                response = client.newCall(request).execute();
                categoriesJSON = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
    public void LoadBills(BillCategory category){
        VoucherHelper voucherHelper = new VoucherHelper(category.CategoryId, category.CategoryName, country, "Utility");
        Intent intent = new Intent(this, SelectBill.class);
        intent.putExtra("voucherHelper", (Serializable) voucherHelper);
        intent.putExtra("categoriesLoaded", categoriesJSON);
        startActivity(intent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

}
