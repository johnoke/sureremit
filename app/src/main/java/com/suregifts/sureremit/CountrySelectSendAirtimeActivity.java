package com.suregifts.sureremit;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.File;

public class CountrySelectSendAirtimeActivity extends AppCompatActivity {
    Toolbar countrySendAirtimeToolBar;
    TextView airtimeCountryText, nigeriaText, kenyaText, sendAirtimeTitleText;
    private DrawerLayout menuDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_select_send_airtime);
        setTitle("");
        countrySendAirtimeToolBar = (Toolbar)findViewById(R.id.CountryAirtimeToolbar);
        setSupportActionBar(countrySendAirtimeToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        airtimeCountryText = (TextView) findViewById(R.id.AirtimeSelectCountryText);
        airtimeCountryText.setText("Select the country you are\nsending airtime to ");
        nigeriaText = (TextView)findViewById(R.id.SendAirtimeNigeriaText);
        kenyaText = (TextView)findViewById(R.id.SendAirtimeKenyaText);
        sendAirtimeTitleText = (TextView)findViewById(R.id.SendAirtimeTitleText);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void LaunchAirtimeNG(View view)
    {
        Intent intent = new Intent(this, SelectAirtime .class);
        String country = "NG";
        intent.putExtra("country", country);
        startActivity(intent);
    }
    public void LaunchAirtimeKE(View view)
    {
        Intent intent = new Intent(this, SelectAirtime.class);
        String country = "KE";
        intent.putExtra("country", country);
        startActivity(intent);
    }
    public void LaunchAirtimeRW(View view)
    {
        Intent intent = new Intent(this, SelectAirtime.class);
        String country = "RW";
        intent.putExtra("country", country);
        startActivity(intent);
    }
}
