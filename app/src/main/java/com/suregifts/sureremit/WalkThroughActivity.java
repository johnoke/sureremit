package com.suregifts.sureremit;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class WalkThroughActivity extends AppCompatActivity {
    ViewPagerAdapter viewPagerAdapter;
    ViewPager mViewPager;
    static final String KEY_IS_FIRST_TIME =  "com.geeks14.sureremit.first_time";
    static final String KEY =  "com.com.geeks14.sureremit";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_through);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new WalkThroughOneFragment(), "Walkthrough One");
        viewPagerAdapter.addFragment(new WalkThroughTwo(), "Walkthrough Two");
        viewPagerAdapter.addFragment(new WalkThroughThreeFragment(), "Walkthrough Three");
        mViewPager = (ViewPager)findViewById(R.id.pager);
        mViewPager.setAdapter(viewPagerAdapter);
        try {
            FileOutputStream fileOutputStream =  getApplicationContext().openFileOutput("sureremit_walkthrough_cache.txt", Context.MODE_WORLD_WRITEABLE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            outputStreamWriter.write("Walked Through");
            outputStreamWriter.flush();
            outputStreamWriter.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();

        }

        setTitle("");
    }
    public void setCurrentItem (int item, boolean smoothScroll) {
        mViewPager.setCurrentItem(item, smoothScroll);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }
        @Override
        public Fragment getItem(int position){
            return fragmentList.get(position);
        }

        @Override
        public int getCount(){
            return fragmentList.size();
        }
        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position){
            return fragmentTitleList.get(position);
        }
    }

}
