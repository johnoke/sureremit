package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/15/2016.
 */
public class VoucherHistory {
    public String _id;
    public String updatedAt;
    public String createdAt;
    public String currency;
    public String locationUrl;
    public String payment;
    public String paymentStatus;
    public String deliveryStatus;
    public String status;
    public String senderEmail;
    public String recipientPhone;
    public String recipientEmail;
    public String recipientName;
    public String amountPayable;
    public String amount;
    public String location;
    public String merchantImage;
    public String merchantId;
    public String merchant;
    public String orderId;
    public String serviceId;
    public String service;
    public String __v;
    public String voucherCode;
    public String category;
    public String referenceNumber;
}

