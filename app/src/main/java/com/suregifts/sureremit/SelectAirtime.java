package com.suregifts.sureremit;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SelectAirtime extends AppCompatActivity {
    private DrawerLayout menuDrawer;
    Toolbar toolbar;
    String country;
    ProgressDialog progressDialog;
    String airtimesJson;
    GridLayout container;
    List<Merchant> airtimesList = new ArrayList<Merchant>();
    VoucherHelper voucherHelper;
    List<Merchant> recentlyUsedMerchants;
    LinearLayout recentlyUsedTab, recentlyUsedContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_airtime);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
        Intent intent = getIntent();
        country = intent.getStringExtra("country");
        container = (GridLayout)findViewById(R.id.AirtimesGrid);
        File file = new File(getFilesDir() + "/sureremit_airtime_cache_" + country +".txt");
        if(file.exists()){
            try{
                airtimesJson = new Scanner(new File(getFilesDir() + "/sureremit_airtime_cache_" + country +".txt")).nextLine();
                DisplayAirtimes();
            }catch(Exception ex){
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please Wait...");
                progressDialog.show();
                new FetchAllAirtimes().execute();
            }

        }else{
            progressDialog = new ProgressDialog(SelectAirtime.this);
            progressDialog.setMessage("Please Wait...");
            progressDialog.show();
            new FetchAllAirtimes().execute();
        }

        airtimesJson = "";
        setTitle("");
        voucherHelper = new VoucherHelper(0, "Airtime", country, "Airtime");
        recentlyUsedContainer = (LinearLayout)findViewById(R.id.RecentlyUsedContainer);
        recentlyUsedTab = (LinearLayout)findViewById(R.id.RecentlyUsedTab);
        recentlyUsedMerchants = Merchant.AirtimeUsedList(this, voucherHelper.country);
        if (recentlyUsedMerchants.size() > 0){
            BindRecentlyUsedMerchants();
            recentlyUsedTab.setVisibility(View.VISIBLE);
        }

    }
    public void BindRecentlyUsedMerchants(){
        for(int i = recentlyUsedMerchants.size() - 1; i >= 0; i--){
            final Merchant merchant = recentlyUsedMerchants.get(i);
            LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View merchantView = layoutInflater.inflate(R.layout.billsdisplay, null);
            CardView merchantContainer = (CardView)merchantView.findViewById(R.id.MerchantContainer);
            ImageView merchantImage = (ImageView)merchantView.findViewById(R.id.MerchantImage);
            TextView merchantText = (TextView)merchantView.findViewById(R.id.MerchantName);
            merchantText.setText(merchant.MerchantName);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageUrl = merchant.ImageName;
            viewHolder.imageView =  merchantImage;
            new DownloadAsyncTask(viewHolder).execute(viewHolder);
            merchantContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voucherHelper.setMerchant_id(merchant.MerchantId);
                    voucherHelper.setMerchant(merchant.MerchantName);
                    voucherHelper.setMerchant_picture(viewHolder.imageUrl);
                    voucherHelper.setCode(merchant.getCode());
                    Intent intent = new Intent(SelectAirtime.this, RecipientDetailsActivity.class);
                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                    startActivity(intent);
                }
            });

            recentlyUsedContainer.addView(merchantView);
        }
    }
    public void DisplayAirtimes()
    {
        try{
            JSONArray jsonArray = new JSONArray(airtimesJson);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                int id = 1;
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int merchantId = jsonObject.getInt("UtilityId");
                String imageUrl = jsonObject.getString("Picture");
                String merchantname = jsonObject.getString("UtilityName");
                String merchantCode = jsonObject.getString("Code");
                String shortDescription = "Buy Airtime";
                String description = "Buy Airtime";
                Merchant merchant = new Merchant(merchantId, merchantname, imageUrl, id, shortDescription, description);
                airtimesList.add(merchant);
                merchant.setCode(merchantCode);
            }
            BindBills();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void BindBills(){
        for(final Merchant merchant:airtimesList){
            LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View merchantView = layoutInflater.inflate(R.layout.billsdisplay, null);
            CardView merchantContainer = (CardView)merchantView.findViewById(R.id.MerchantContainer);
            ImageView merchantImage = (ImageView)merchantView.findViewById(R.id.MerchantImage);
            TextView merchantText = (TextView)merchantView.findViewById(R.id.MerchantName);
            merchantText.setText(merchant.MerchantName);
            final ViewHolder viewHolder = new ViewHolder();
            if (country.equals("NG")){
                viewHolder.imageUrl = "http://cms.suregifts.com.ng/utilitypics/" + merchant.ImageName;
            }
            else if(country.equals("KE")){
                viewHolder.imageUrl = "http://cms.suregifts.co.ke/utilitypics/" + merchant.ImageName;
            }
            else{
                viewHolder.imageUrl = "http://cms.suregifts.rw/utilitypics/" + merchant.ImageName;
            }
            viewHolder.imageView =  merchantImage;
            new DownloadAsyncTask(viewHolder).execute(viewHolder);
            merchantContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    voucherHelper.setMerchant_id(merchant.MerchantId);
                    voucherHelper.setMerchant(merchant.MerchantName);
                    voucherHelper.setMerchant_picture(viewHolder.imageUrl);
                    voucherHelper.setCode(merchant.getCode());
                    Intent intent = new Intent(SelectAirtime.this, RecipientDetailsActivity.class);
                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                    startActivity(intent);
                }
            });
            container.addView(merchantView);
        }
    }
    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        private int position;
        public DownloadAsyncTask(ViewHolder viewHolder)
        {

        }
        @Override
        protected  ViewHolder doInBackground(ViewHolder... params){
            ViewHolder viewHolder = params[0];

            try{
                URL imageUrl = new URL(viewHolder.imageUrl);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageUrl.openStream());
            }catch(IOException e){
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }
        @Override
        protected void onPostExecute(ViewHolder result){
            if (result.bitmap == null){
                result.imageView.setImageResource(android.R.color.transparent);
            }else{
                result.imageView.setImageBitmap(result.bitmap);
            }

        }
    }
    public class FetchAllAirtimes extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            try {
                FileOutputStream fileOutputStream =  getApplicationContext().openFileOutput("sureremit_airtime_cache_" + country +".txt", Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(airtimesJson);
                outputStreamWriter.flush();
                outputStreamWriter.close();

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            DisplayAirtimes();
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String utilUrl = "";
            if (country.equals("NG")){
                utilUrl = "http://cms.suregifts.com.ng/api/utilities/phone";
            }else if(country.equals("KE")){
                utilUrl = "http://cms.suregifts.co.ke/api/utilities/phone";
            }else{
                utilUrl = "http://cms.suregifts.rw/api/utilities/phone";
            }
            request = new Request.Builder()
                    .url(utilUrl)
                    .build();
            Response response;
            try{
                response = client.newCall(request).execute();
                airtimesJson = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    public void CloseNavigationDrawer(View view){
        try {

            menuDrawer.closeDrawer(GravityCompat.START);

        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private static class ViewHolder {
        ImageView imageView;
        TextView name;
        String datetime;
        String imageUrl;
        Bitmap bitmap;
        int id;
    }
}
