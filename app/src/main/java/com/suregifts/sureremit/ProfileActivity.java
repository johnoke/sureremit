package com.suregifts.sureremit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity {
    FontTextView fullNameTxt;
    Toolbar profileToolbar;
    private EditText email, firstname, lastname, phonenumber, city, zipCode, address;
    private TextInputLayout emailLayout, firstnameLayout, lastnameLayout, phonenumberLayout, cityLayout, zipCodeLayout, addressLayout;
    private Spinner countrySpinner;
    private AppCompatButton submitButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    UserProfile userProfile;
    ProgressDialog progressDialog;
    UserHelper userHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        profileToolbar = (Toolbar)findViewById(R.id.ProfileToolbar);
        setSupportActionBar(profileToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        email = (EditText)findViewById(R.id.ProfileEmail);
        firstname = (EditText)findViewById(R.id.ProfileFirstName);
        lastname = (EditText)findViewById(R.id.ProfileLastName);
        city = (EditText)findViewById(R.id.ProfileCity);
        zipCode = (EditText)findViewById(R.id.ProfileZipCode);
        address = (EditText)findViewById(R.id.ProfileAddress);
        phonenumber = (EditText)findViewById(R.id.ProfilePhone);
        phonenumber = (EditText)findViewById(R.id.ProfilePhone);
        emailLayout = (TextInputLayout)findViewById(R.id.ProfileEmailLayout);
        firstnameLayout = (TextInputLayout)findViewById(R.id.ProfileFirstNameLayout);
        submitButton = (AppCompatButton)findViewById(R.id.EditProfileButton);
        lastnameLayout = (TextInputLayout)findViewById(R.id.ProfileLastNameLayout);
        phonenumberLayout = (TextInputLayout)findViewById(R.id.ProfilePhoneLayout);
        cityLayout = (TextInputLayout)findViewById(R.id.ProfileCityLayout);
        zipCodeLayout = (TextInputLayout)findViewById(R.id.ZipCodeLayout);
        addressLayout = (TextInputLayout)findViewById(R.id.AddressLayout);

        countrySpinner = (Spinner)findViewById(R.id.ProfileCountry);
        progressDialog = new ProgressDialog(this);

        email.addTextChangedListener(new MyTextWatcher(email));
        firstname.addTextChangedListener(new MyTextWatcher(firstname));
        lastname.addTextChangedListener(new MyTextWatcher(lastname));

        phonenumber.addTextChangedListener(new MyTextWatcher(phonenumber));
        userProfile = new UserProfile("","","","","");
        GetUserData();
        email.setText(userProfile.Email);
        firstname.setText(userProfile.FirstName);
        lastname.setText(userProfile.LastName);
        phonenumber.setText(userProfile.PhoneNumber);
        city.setText(userProfile.City);
        zipCode.setText(userProfile.ZipCode);
        address.setText(userProfile.Address);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validateFirstName() && validateLastName() && validatePhoneNumber() && validateCity() && validateZipCode() && validateAddress()) {
                    userHelper = new UserHelper();
                    userHelper.setFirstname(firstname.getText().toString());
                    userHelper.setLastname(lastname.getText().toString());
                    userHelper.setEmail(email.getText().toString());
                    userHelper.setPhone(phonenumber.getText().toString());
                    userHelper.setCountry(userProfile.Country);
                    userHelper.setCity(city.getText().toString());
                    userHelper.setZipcode(zipCode.getText().toString());
                    userHelper.setAddress(address.getText().toString());
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Saving Changes");
                    progressDialog.show();
                    new AsyncProfileUpdate().execute();
                }
            }
        });
        final String[] countries = LoadCountries();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.countryspinnerrow, R.id.PackageName, countries);

        countrySpinner.setAdapter(adapter);
        try{
            if(!userProfile.Country.equals(null)){
                int spinnerPosition = adapter.getPosition(userProfile.Country);
                Log.e("Country SpinnerPosition", String.valueOf(spinnerPosition));
                countrySpinner.setSelection(spinnerPosition);
            }else{
                int spinnerPosition = GetPosition("Nigeria", countries);
                Log.e("Default SpinnerPosition", String.valueOf(spinnerPosition));
                countrySpinner.setSelection(spinnerPosition);
            }
        }catch(Exception ex){

        }
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userProfile.Country = countries[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setTitle("");

    }
    private boolean validateEmail(){
        String strEmail = email.getText().toString();
        if (strEmail.equals("") || strEmail.equals(null)) {
            emailLayout.setError("Email is required");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateCity(){
        String strCity = city.getText().toString();
        if (strCity.equals("") || strCity.equals(null)) {
            cityLayout.setError("City is required");
            cityLayout.requestFocus();
            cityLayout .setErrorEnabled(true);
            return false;
        } else {
            cityLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateZipCode(){
        String strZipCode = zipCode.getText().toString();
        if (strZipCode.equals("") || strZipCode.equals(null)) {
            zipCodeLayout.setError("Zip Code is required");
            zipCodeLayout.requestFocus();
            zipCodeLayout .setErrorEnabled(true);
            return false;
        } else {
            cityLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateFirstName(){
        String strFirstName = firstname.getText().toString();
        if (strFirstName.equals("") || strFirstName.equals(null)) {
            firstnameLayout.setError("First Name is required");
            firstnameLayout.requestFocus();
            firstnameLayout .setErrorEnabled(true);
            return false;
        } else {
            firstnameLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateLastName(){
        String strLastName = lastname.getText().toString();
        if (strLastName.equals("") || strLastName.equals(null)) {
            lastnameLayout.setError("Last Name is required");
            lastnameLayout.requestFocus();
            lastnameLayout.setErrorEnabled(true);
            return false;
        } else {
            lastnameLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateAddress(){
        String strAddress = address.getText().toString();
        if (strAddress.equals("") || strAddress.equals(null)) {
            addressLayout.setError("Address is required");
            addressLayout.requestFocus();
            addressLayout.setErrorEnabled(true);
            return false;
        } else {
            addressLayout.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatePhoneNumber(){
        String strPhoneNumber = phonenumber.getText().toString();
        if (strPhoneNumber.equals("") || strPhoneNumber.equals(null)) {
            phonenumberLayout.setError("Phone Number is required");
            phonenumberLayout.requestFocus();
            phonenumberLayout.setErrorEnabled(true);
            return false;
        } else {
            phonenumberLayout.setErrorEnabled(false);
        }

        return true;
    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.ProfileEmail:
                    validateEmail();
                    break;

                case R.id.ProfileFirstName:
                    validateFirstName();
                    break;
                case R.id.ProfileLastName:
                    validateLastName();
                    break;

            }
        }
    }

    public class AsyncProfileUpdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String result;
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(userHelper);
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/profile/update")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
            }
            catch(IOException ex)
            {
                result = "Server Error";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject obj = new JSONObject(s);
                String status = obj.getString("status");
                if(status.equals("0")){
                    String phone = "";
                    UserProfile profile = new UserProfile(userHelper.firstname, userHelper.lastname, userHelper.email, "", userHelper.phone);
                    profile.Country = userProfile.Country;
                    profile.City = userHelper.city;
                    profile.ZipCode = userHelper.zipcode;
                    profile.Address = userHelper.address;
                    Gson gson = new GsonBuilder().create();
                    String userData = gson.toJson(profile);
                    try {
                        FileOutputStream fileOutputStream =  getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(userData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        DisplaySuccessPopUp("Profile Saved Successfully", "Changes made to your profile was saved successfully");
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
                    }
                } else {
                    DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
                }
            } catch (Exception ex) {
                DisplayErrorPopUp("Something went wrong", "We were unable to save your profile details, Please try again later");
            }

            progressDialog.dismiss();

        }
    }
    public void DisplayErrorPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void DisplaySuccessPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        TextView dismissButton = (TextView)dialog.findViewById(R.id.PopUpDismissButton);
        dismissButton.setText("Back to Main Menu");
        popupImage.setImageResource(R.drawable.check);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_logout:
                Logout();
                break;
            case R.id.action_main_menu:
                ToMenu();
                break;
            case R.id.action_terms:
                Terms();
                break;
            case R.id.action_about:
                About();
                break;
            case R.id.action_Privacy:
                Privacy();
                break;
            default:
                finish();
                return true;
        }
        return true;
    }
    public void Logout(){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void GetUserData()
    {
        try{
            String rawdata = new Scanner(new File(getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            Log.e("Login Response", rawdata);
            userProfile.Email = obj.getString("Email");
            userProfile.FirstName = obj.getString("FirstName");
            userProfile.LastName = obj.getString("LastName");
            userProfile.PhoneNumber = obj.getString("PhoneNumber");
            userProfile.Country = obj.getString("Country");
            userProfile.City = obj.getString("City");
            userProfile.ZipCode = obj.getString("ZipCode");
            userProfile.Address = obj.getString("Address");
            Log.e("Login Response", rawdata);

        }
        catch (Exception ex){

        }

    }
    public String[] LoadCountries(){
        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale locale: locales) {
            country = locale.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
        String[] countriesArr = countries.toArray(new String[countries.size()]);
        return countriesArr;
    }
    public int GetPosition(String value, String[] array){
        int data = 0;
        for (int i = 0; i < array.length; i++){
            if (array[i].equals(value)){
                data = i;
                return data ;
            }
        }
        return data;
    }
}
