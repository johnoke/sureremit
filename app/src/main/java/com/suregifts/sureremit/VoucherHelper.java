package com.suregifts.sureremit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SUREGIFTS-DEVPC on 5/20/2016.
 */
public class VoucherHelper implements Serializable{
    public int category_id;
    public String category;
    public int merchant_id;
    public String merchant;
    public String merchant_picture;
    public String location;
    public String amount;
    public String amount_payable;
    public String recipient_name;
    public String recipient_email;
    public String recipient_phone;
    public String email;
    public String special_message;
    public String delivery_mode;
    public String payment_id;
    public String merchant_label;
    public String package_number;
    public String user_id;
    public String country;
    public String code;
    public String optype;
    public String name;
    public List<BillPackage> BillPackages;
    public VoucherHelper(){};
    public VoucherHelper(int category_id, String category, String country, String optype){
        this.category_id = category_id;
        this.category = category;
        this.country = country;
        this.optype = optype;
        this.BillPackages = new ArrayList<BillPackage>();
    }
    public int getCategory_id(){ return this.category_id; }
    public void setCategory_id(int category_id){this.category_id = category_id; }
    public String getCategory(){ return this.category; }
    public void setCategory(String category){this.category = category; };
    public int getMerchant_id(){ return this.merchant_id; }
    public void setMerchant_id(int merchant_id){this.merchant_id = merchant_id; }
    public String getMerchant(){ return this.merchant; }
    public void setMerchant(String merchant){this.merchant = merchant; };
    public String getMerchant_picture(){ return this.merchant_picture; }
    public void setMerchant_picture(String merchant_picture){this.merchant_picture = merchant_picture;}
    public String getLocation(){ return this.location; }
    public void setLocation(String location){this.location = location;}
    public String getAmount(){ return this.amount; }
    public void setAmount(String amount){this.amount = amount;}
    public String getAmount_payable(){ return this.amount_payable; }
    public void setAmount_payable(String amount_payable){this.amount_payable = amount_payable;}
    public String getRecipient_name(){ return this.recipient_name; }
    public void setRecipient_name(String recipient_name){this.recipient_name = recipient_name; }
    public String getRecipient_email(){ return this.recipient_email; }
    public void setRecipient_email(String recipient_email){this.recipient_email = recipient_email; }
    public String getRecipient_phone(){ return this.recipient_phone; }
    public void setRecipient_phone(String recipient_phone){this.recipient_phone = recipient_phone; }
    public String getEmail(){ return this.email; }
    public void setEmail(String email){this.email = email; }
    public String getPayment_id(){ return this.payment_id; }
    public void setPayment_id(String payment_id){this.payment_id = payment_id; }
    public String getUser_Id(){ return this.user_id; }
    public void setUser_Id(String user_id){this.user_id = user_id; }
    public String getSpecial_message(){ return this.special_message; }
    public void setSpecial_message(String special_message){this.special_message = special_message; }
    public String getDelivery_mode(){ return this.delivery_mode; }
    public void setDelivery_mode(String delivery_mode){this.delivery_mode = delivery_mode; }
    public String getCountry(){ return this.country; }
    public void setCountry(String country){this.country = country; }
    public String getOptype(){ return this.optype; }
    public void setOptype(String optype){this.optype = optype; }
    public String getMerchant_label(){ return this.merchant_label; }
    public void setMerchant_label(String merchant_label){this.merchant_label = merchant_label; }
    public String getPackage_number(){ return this.package_number; }
    public void setPackage_number(String package_number){this.package_number = package_number; }
    public String getCode(){ return this.code; }
    public void setCode(String code){this.code = code; }
    public List<BillPackage> getBillPackages(){return this.BillPackages; }
    public void setBillPackages(List<BillPackage> billPackages){  this.BillPackages = billPackages; }

}
