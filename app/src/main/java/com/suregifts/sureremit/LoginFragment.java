package com.suregifts.sureremit;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    EditText email, password;
    TextInputLayout emailLayout, passwordLayout;
    FontRegularTextView loginErrorMessage;
    AppCompatButton submitButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    AccessToken accessToken;
    private String loginResult;
    UserProfile userProfile;
    ProgressDialog progressDialog;
    UserHelper userHelper;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        email = (EditText)v.findViewById(R.id.LogInEmail);
        password = (EditText)v.findViewById(R.id.LoginPassword);
        emailLayout = (TextInputLayout)v.findViewById(R.id.LogInEmailLayout);
        passwordLayout = (TextInputLayout)v.findViewById(R.id.LoginPasswordLayout);
        submitButton = (AppCompatButton)v.findViewById(R.id.LoginButton);
        email.addTextChangedListener(new MyTextWatcher(email));
        password.addTextChangedListener(new MyTextWatcher(password));
        progressDialog = new ProgressDialog(getActivity());
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Submit();
            }
        });
        callbackManager = CallbackManager.Factory.create();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
              //  nextActivity(newProfile);
            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
        profileTracker.startTracking();
        userProfile = new UserProfile("", "", "", "", "");
        LoginButton loginButton = (LoginButton)v.findViewById(R.id.LoginFBButton);
        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try{
                            String uid = loginResult.getAccessToken().getUserId().toString();
                            String email = object.getString("email");
                            String name = object.getString("name");
                            String[] names = name.split(" ");
                            String firstname =  "";
                            String lastname = "";
                            try{
                                firstname = names[0];
                                lastname = names[1];
                            }catch(Exception ex){
                                firstname = name;
                            }
                            userHelper = new UserHelper(uid, email, firstname, lastname);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setCancelable(false);
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();
                            new AsyncFacebookCallBack().execute();
                        }catch(Exception ex){
                            ex.printStackTrace();
                            DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        loginErrorMessage = (FontRegularTextView)v.findViewById(R.id.LoginErrMsg);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onResume(){
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //nextActivity(profile);
    }
    private boolean validateEmail(){
        String strEmail = email.getText().toString();
        String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        boolean validateEmail = strEmail.matches(EMAIL_REGEX);
        if (strEmail.equals("") || strEmail.equals(null)) {
            emailLayout.setError("Email is required");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        }else if(!validateEmail){
            emailLayout.setError("Email is not in the right format");
            emailLayout.requestFocus();
            emailLayout.setErrorEnabled(true);
            return false;
        } else {
            emailLayout.setError(null);
        }

        return true;
    }
    private boolean validatePassword(){
        String strPassword = password.getText().toString();
        if (strPassword.equals("") || strPassword.equals(null)) {
            passwordLayout.setError("Password is required");
            passwordLayout.requestFocus();
            passwordLayout.setErrorEnabled(true);
            return false;
        } else {
            passwordLayout.setError(null);
        }

        return true;
    }
    public void Submit(){
        if(validateEmail() && validatePassword()){
            try{
//                progressDialog.setIndeterminate(true);
//                progressDialog.setCancelable(false);
//                progressDialog.setMessage("Validating...");
//                progressDialog.show();
//                userProfile.Email = email.getText().toString();
//                userProfile.Password = password.getText().toString();
//                AsyncTaskPostLogin asyncTaskPostLogin = new AsyncTaskPostLogin();
//                asyncTaskPostLogin.execute();
                UserHelper userHelper = new UserHelper();
                Gson gson = new GsonBuilder().create();
                UserProfile profile = new UserProfile("John", "Oke", "john@wallet.ng", "", "2347068264085");
                profile.Address = userHelper.address;
                profile.City = userHelper.city;
                profile.ZipCode = userHelper.zipcode;
                profile.Country = userHelper.country;
                String userData = gson.toJson(profile);
                try {
                    FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    outputStreamWriter.write(userData);
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    LaunchMenu();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }
    public class AsyncFacebookCallBack extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {
            String result;
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(userHelper);
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/auth/facebook")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
            }
            catch(IOException ex)
            {
                result = "Server Error";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject obj = new JSONObject(s);
                String status = obj.getString("status");
                if(status.equals("0")){
                    //String phone = "";
                    Gson gson = new GsonBuilder().create();
                    String email = obj.getString("email");
                    String firstname = obj.getString("firstname");
                    String lastname = obj.getString("lastname");
                    String phone = obj.getString("phone");
                    UserHelper userHelper = new UserHelper();
                    try {
                        userHelper.city = obj.getString("city");
                        userHelper.country = obj.getString("country");
                        userHelper.zipcode = obj.getString("zipcode");
                        userHelper.address = obj.getString("address");
                    }catch (Exception e){

                    }

                    userHelper.email = email;
                    userHelper.firstname = firstname;
                    userHelper.lastname = lastname;
                    userHelper.phone = phone;
                    UserProfile profile = new UserProfile(userHelper.firstname, userHelper.lastname, userProfile.Email, "", userHelper.phone);
                    profile.Address = userHelper.address;
                    profile.City = userHelper.city;
                    profile.ZipCode = userHelper.zipcode;
                    profile.Country = userHelper.country;
                    String userData = gson.toJson(profile);
                    try {
                        FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(userData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        LaunchMenu();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        loginResult = "Internal App Error";
                        DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                    }


                } else {
                    DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
                }
            } catch (Exception ex) {
                DisplayErrorPopUp("Facebook Error", "Facebook is currently not available, Please sign in using your email address and passsword");
            }
            LoginManager.getInstance().logOut();
            progressDialog.dismiss();
        }
    }

    public class AsyncTaskPostLogin extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                Gson gson = new GsonBuilder().create();
                JSONObject obj = new JSONObject(loginResult);
                String status = obj.getString("status");
                loginResult = status;
                if (status.equals("0")){
                    String email = obj.getString("email");
                    String firstname = obj.getString("firstname");
                    String lastname = obj.getString("lastname");
                    String phone = obj.getString("phone");
                    UserHelper userHelper = new UserHelper();
                    try {
                        userHelper.city = obj.getString("city");
                        userHelper.country = obj.getString("country");
                        userHelper.zipcode = obj.getString("zipcode");
                        userHelper.address = obj.getString("address");
                    }catch (Exception e){

                    }

                    userHelper.email = email;
                    userHelper.firstname = firstname;
                    userHelper.lastname = lastname;
                    userHelper.phone = phone;
                    UserProfile profile = new UserProfile(userHelper.firstname, userHelper.lastname, userProfile.Email, "", userHelper.phone);
                    profile.Address = userHelper.address;
                    profile.City = userHelper.city;
                    profile.ZipCode = userHelper.zipcode;
                    profile.Country = userHelper.country;
                    String userData = gson.toJson(profile);
                    try {
                        FileOutputStream fileOutputStream =  getActivity().getApplicationContext().openFileOutput("sureremit_user_cache.txt", Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(userData);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        LaunchMenu();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        loginResult = "Internal App Error";
                        DisplayError(loginResult);
                    }

                }
                else{
                    String loginapimsg = obj.getString("message");
                    DisplayError(loginapimsg);
                }

            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                loginResult = "Internal Server Error";
                DisplayError(loginResult);
            }
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String json = "{\"email\" : \"" + userProfile.Email + "\", \"password\" : \"" + userProfile.Password + "\"}";
                RequestBody body = RequestBody.create(JSON, json);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/login")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                loginResult = response.body().string();
                Log.e("Login Response", loginResult);
            }
            catch(IOException ex)
            {
                loginResult = "Server Error";
            }
            return null;
        }

    }
    public void DisplayError(String errMsg)
    {
        loginErrorMessage.setText("The email address and password do not match.\n" +
                "Please try a different combination.");
        loginErrorMessage.setVisibility(View.VISIBLE);
    }
    public void LaunchMenu()
    {
        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
    }

    class MyTextWatcher implements TextWatcher{
        private View view;
        private MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.LogInEmail:
                    validateEmail();
                    break;
                case R.id.LoginPassword:
                    validatePassword();
            }
        }
    }

    public void DisplayErrorPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
