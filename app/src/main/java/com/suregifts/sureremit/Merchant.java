package com.suregifts.sureremit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by SUREGIFTS-DEVPC on 5/23/2016.
 */
public class Merchant {
    public int MerchantId;
    public String MerchantName;
    public String ImageName;
    public String ShortDescription;
    public String Description;
    public int CategoryId;
    public List<BillPackage> BillPackages;
    public String Label;
    public String Code;
    public String Country;
    public String Locations;
    public String RedemptionDetails;
    public Merchant(int merchantId, String merchantName, String imageName, int categoryId, String shortDescription, String description){
        this.MerchantId = merchantId;
        this.MerchantName = merchantName;
        this.ImageName = imageName;
        this.CategoryId = categoryId;
        this.ShortDescription = shortDescription;
        this.Description = description;
        this.BillPackages = new ArrayList<BillPackage>();
    }
    public List<BillPackage> getBillPackages(){return this.BillPackages; }
    public void setBillPackages(List<BillPackage> billPackages){  this.BillPackages = billPackages; }
    public String getLabel(){return this.Label; }
    public void setLabel(String label){  this.Label = label; }
    public String getCode(){return this.Code; }
    public void setCode(String code){this.Code = code; }
    public void setCountry(String country){this.Country = country;}
    public String getCountry(){return this.Country;}
    public void setLocations(String locations){this.Locations = locations;}
    public String getLocations(){return this.Locations;}
    public void setRedemptionDetails(String redemptionDetails){this.RedemptionDetails = redemptionDetails;}
    public String getRedemptionDetails(){return this.RedemptionDetails;}
    public static boolean AddVoucherHistory(Merchant merchant, Context context){
        String filename = merchant.Country.equals("NG") ? "sureremit_voucher_history_cache_ng.txt" : "sureremit_voucher_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/" + filename)).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                List<Merchant> merchants = VoucherUsedList(context, merchant.getCountry());
                boolean exists = false;
                for (Merchant merch: merchants){
                    if(merch.MerchantId == merchant.MerchantId){
                        exists = true;
                        break;
                    }
                }
                if(!exists) {
                    merchants.add(merchant);
                    Gson gson = new GsonBuilder().create();

                    File voucherHistoryFile = new File(context.getFilesDir() + "/" + filename);
                    voucherHistoryFile.delete();
                    String voucherHistoryJson = gson.toJson(merchants);
                    try {
                        FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(voucherHistoryJson);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        return true;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return false;
                    }
                }
                else{
                    return true;
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else {
            List<Merchant> merchants = new ArrayList<Merchant>();
            merchants.add(merchant);
            Gson gson = new GsonBuilder().create();
            String recipientJSON  = gson.toJson(merchants);
            try {
                FileOutputStream fileOutputStream =  context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(recipientJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();
                return true;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
        }
    }
    public static List<Merchant> VoucherUsedList(Context context, String country){
        List<Merchant> merchants = new ArrayList<Merchant>();
        String filename = country.equals("NG") ? "sureremit_voucher_history_cache_ng.txt" : "sureremit_voucher_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);

        if(file.exists()){
            try{
                String rawdata = new Scanner(file).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int merchantId = jsonObject.getInt("MerchantId");
                    String merchantName = jsonObject.getString("MerchantName");
                    String imageName = jsonObject.getString("ImageName");
                    int categoryId = jsonObject.getInt("CategoryId");
                    String shortDescription = jsonObject.getString("ShortDescription");
                    String description = jsonObject.getString("Description");
                    String label = "";
                    String code = "";
                    List<BillPackage> packages = new ArrayList<BillPackage>();
                    try{
                        label = jsonObject.getString("Label");
                        code = jsonObject.getString("Code");
                        JSONArray merchantPackages = jsonObject.getJSONArray("BillPackages");
                        for (int j = 0; j < merchantPackages.length(); j++){
                            JSONObject packageObject = merchantPackages.getJSONObject(j);
                            String packageName = packageObject.getString("PackageName");
                            Double packageAmount = packageObject.getDouble("Amount");
                            BillPackage billPackage = new BillPackage();
                            packages.add(billPackage);
                        }
                    }catch (Exception ex){

                    }
                    Merchant merchant = new Merchant(merchantId, merchantName,imageName, categoryId, shortDescription, description);
                    merchant.setBillPackages(packages);
                    merchant.setCode(code);
                    merchant.setLabel(label);
                    merchants.add(merchant);
                }

            }
            catch(Exception ex){
                ex.printStackTrace();

            }
        }
        return merchants;
    }
    public static boolean AddBillHistory(Merchant merchant, Context context){
        String filename = merchant.Country.equals("NG") ? "sureremit_bill_history_cache_ng.txt" : "sureremit_bill_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/" + filename)).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                List<Merchant> merchants = BillUsedList(context, merchant.getCountry());
                boolean exists = false;
                for (Merchant merch: merchants){
                    if(merch.MerchantId == merchant.MerchantId){
                        exists = true;
                        break;
                    }
                }
                if(!exists) {
                    merchants.add(merchant);
                    Gson gson = new GsonBuilder().create();

                    File voucherHistoryFile = new File(context.getFilesDir() + "/" + filename);
                    voucherHistoryFile.delete();
                    String voucherHistoryJson = gson.toJson(merchants);
                    try {
                        FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(voucherHistoryJson);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        return true;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return false;
                    }
                }
                else{
                    return true;
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else {
            List<Merchant> merchants = new ArrayList<Merchant>();
            merchants.add(merchant);
            Gson gson = new GsonBuilder().create();
            String recipientJSON  = gson.toJson(merchants);
            try {
                FileOutputStream fileOutputStream =  context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(recipientJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();
                return true;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
        }
    }
    public static List<Merchant> BillUsedList(Context context, String country){
        List<Merchant> merchants = new ArrayList<Merchant>();
        String filename = country.equals("NG") ? "sureremit_bill_history_cache_ng.txt" : "sureremit_bill_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);

        if(file.exists()){
            try{
                String rawdata = new Scanner(file).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int merchantId = jsonObject.getInt("MerchantId");
                    String merchantName = jsonObject.getString("MerchantName");
                    String imageName = jsonObject.getString("ImageName");
                    int categoryId = jsonObject.getInt("CategoryId");
                    String shortDescription = jsonObject.getString("ShortDescription");
                    String description = jsonObject.getString("Description");
                    String label = "";
                    String code = "";
                    List<BillPackage> packages = new ArrayList<BillPackage>();
                    try{
                        label = jsonObject.getString("Label");
                        code = jsonObject.getString("Code");
                        JSONArray merchantPackages = jsonObject.getJSONArray("BillPackages");
                        for (int j = 0; j < merchantPackages.length(); j++){
                            JSONObject packageObject = merchantPackages.getJSONObject(j);
                            String packageName = packageObject.getString("PackageName");
                            Double packageAmount = packageObject.getDouble("Amount");
                            BillPackage billPackage = new BillPackage();
                            packages.add(billPackage);
                        }
                    }catch (Exception ex){

                    }
                    Merchant merchant = new Merchant(merchantId, merchantName,imageName, categoryId, shortDescription, description);
                    merchant.setBillPackages(packages);
                    merchant.setCode(code);
                    merchant.setLabel(label);
                    merchants.add(merchant);
                }

            }
            catch(Exception ex){
                ex.printStackTrace();

            }
        }
        return merchants;
    }
    public static boolean AddAirtimeHistory(Merchant merchant, Context context){
        String filename = merchant.Country.equals("NG") ? "sureremit_airtime_history_cache_ng.txt" : "sureremit_airtime_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);
        if(file.exists()){
            try{
                String rawdata = new Scanner(new File(context.getFilesDir() + "/" + filename)).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                List<Merchant> merchants = AirtimeUsedList(context, merchant.getCountry());
                boolean exists = false;
                for (Merchant merch: merchants){
                    if(merch.MerchantId == merchant.MerchantId){
                        exists = true;
                        break;
                    }
                }
                if(!exists) {
                    merchants.add(merchant);
                    Gson gson = new GsonBuilder().create();

                    File voucherHistoryFile = new File(context.getFilesDir() + "/" + filename);
                    voucherHistoryFile.delete();
                    String voucherHistoryJson = gson.toJson(merchants);
                    try {
                        FileOutputStream fileOutputStream = context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        outputStreamWriter.write(voucherHistoryJson);
                        outputStreamWriter.flush();
                        outputStreamWriter.close();
                        return true;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return false;
                    }
                }
                else{
                    return true;
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                return false;
            }
        }
        else {
            List<Merchant> merchants = new ArrayList<Merchant>();
            merchants.add(merchant);
            Gson gson = new GsonBuilder().create();
            String recipientJSON  = gson.toJson(merchants);
            try {
                FileOutputStream fileOutputStream =  context.openFileOutput(filename, Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(recipientJSON);
                outputStreamWriter.flush();
                outputStreamWriter.close();
                return true;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                return false;
            }
        }
    }
    public static List<Merchant> AirtimeUsedList(Context context, String country){
        List<Merchant> merchants = new ArrayList<Merchant>();
        String filename = country.equals("NG") ? "sureremit_airtime_history_cache_ng.txt" : "sureremit_airtime_history_cache_ke.txt";
        File file = new File(context.getFilesDir() + "/" + filename);

        if(file.exists()){
            try{
                String rawdata = new Scanner(file).nextLine();
                JSONArray jsonArray = new JSONArray(rawdata);
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int merchantId = jsonObject.getInt("MerchantId");
                    String merchantName = jsonObject.getString("MerchantName");
                    String imageName = jsonObject.getString("ImageName");
                    int categoryId = jsonObject.getInt("CategoryId");
                    String shortDescription = jsonObject.getString("ShortDescription");
                    String description = jsonObject.getString("Description");
                    String label = "";
                    String code = "";
                    List<BillPackage> packages = new ArrayList<BillPackage>();
                    try{
                        label = jsonObject.getString("Label");
                        code = jsonObject.getString("Code");
                        JSONArray merchantPackages = jsonObject.getJSONArray("BillPackages");
                        for (int j = 0; j < merchantPackages.length(); j++){
                            JSONObject packageObject = merchantPackages.getJSONObject(j);
                            String packageName = packageObject.getString("PackageName");
                            Double packageAmount = packageObject.getDouble("Amount");
                            BillPackage billPackage = new BillPackage();
                            packages.add(billPackage);
                        }
                    }catch (Exception ex){

                    }
                    Merchant merchant = new Merchant(merchantId, merchantName,imageName, categoryId, shortDescription, description);
                    merchant.setBillPackages(packages);
                    merchant.setCode(code);
                    merchant.setLabel(label);
                    merchants.add(merchant);
                }

            }
            catch(Exception ex){
                ex.printStackTrace();

            }
        }
        return merchants;
    }
}
