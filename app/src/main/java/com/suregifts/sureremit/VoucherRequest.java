package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/2/2016.
 */
public class VoucherRequest {
    public String location;
    public String recipient_name;
    public String recipient_phone;
    public String recipient_email;
    public String merchant;
    public String merchant_id;
    public String amount_payable;
    public String amount;
    public String email;
    public String payment_id;
    public String merchant_image;
    public String category;
    public String name;
    public VoucherRequest(String location, String recipient_name, String recipient_phone,
                            String recipient_email, String merchant, String merchant_id, String amount_payable,
                            String amount, String email, String payment_id,String merchant_image){
        this.location = location;
        this.recipient_name = recipient_name;
        this.recipient_phone = recipient_phone;
        this.recipient_email = recipient_email;
        this.merchant = merchant;
        this.merchant_id = merchant_id;
        this.amount_payable = amount_payable;
        this.amount = amount;
        this.email = email;
        this.payment_id = payment_id;
        this.merchant_image = merchant_image;
    }
}
