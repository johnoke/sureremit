package com.suregifts.sureremit;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SUREGIFTS-DEVPC on 7/7/2016.
 */
public class BillCategory implements Serializable {
    public int CategoryId;
    public String CategoryName;
    public String Pictureurl;
    public int Count;
    public int PictureId;
    public List<Bill> Bills;

    public int ID;
    public String Name;
    public String ImageUrl;
}
