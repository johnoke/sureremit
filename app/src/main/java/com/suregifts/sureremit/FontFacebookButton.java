package com.suregifts.sureremit;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.facebook.login.widget.LoginButton;

/**
 * Created by SUREGIFTS-DEVPC on 5/17/2016.
 */
public class FontFacebookButton extends LoginButton {

    public FontFacebookButton(Context context) {
        super(context);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Lato-Bold.ttf");
        this.setTypeface(face);
    }

    public FontFacebookButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Lato-Bold.ttf");
        this.setTypeface(face);
    }

    public FontFacebookButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Lato-Bold.ttf");
        this.setTypeface(face);
    }
}
