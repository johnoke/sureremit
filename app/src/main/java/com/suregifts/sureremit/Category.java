package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 5/19/2016.
 */
public class Category {
    public int CategoryId;
    public String CategoryName;
    public String ImageName;
    public int ImageId;
    public int NumberofProducts;
    public Category(int categoryId, String categoryName, String imageName, int imageId, int numberofProducts){
        this.CategoryId = categoryId;
        this.CategoryName = categoryName;
        this.ImageName = imageName;
        this.ImageId = imageId;
        this.NumberofProducts = numberofProducts;
    }
}
