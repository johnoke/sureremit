package com.suregifts.sureremit;

import java.io.Serializable;

/**
 * Created by SUREGIFTS-DEVPC on 6/1/2016.
 */
public class BillPackage implements Serializable {
    public int PackageId;
    public double Amount;
    public String Label;
    public String PaymentCode;

}
