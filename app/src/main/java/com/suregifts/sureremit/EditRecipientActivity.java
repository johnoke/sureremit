package com.suregifts.sureremit;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;

public class EditRecipientActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextInputLayout recipientEmailLayout;
    TextInputLayout recipientPhoneLayout;
    TextInputLayout recipientFullNameLayout;
    FontEditText recipientEmail;
    FontEditText recipientPhone;
    FontEditText recipientFullName;
    VoucherHelper voucherHelper;
    Recipient tobeEditedRecipient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipient);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable upArrow = getResources().getDrawable(R.drawable.close);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        recipientEmailLayout = (TextInputLayout)findViewById(R.id.RecipientEmailLayout);
        recipientPhoneLayout = (TextInputLayout)findViewById(R.id.RecipientPhoneLayout);
        recipientFullNameLayout = (TextInputLayout)findViewById(R.id.RecipientFullNameLayout);
        recipientEmail = (FontEditText)findViewById(R.id.RecipientEmail);
        recipientPhone = (FontEditText)findViewById(R.id.RecipientPhone);
        recipientFullName = (FontEditText)findViewById(R.id.RecipientFullName);
        recipientPhone.addTextChangedListener(new MyTextWatcher(recipientPhone));
        recipientEmail.addTextChangedListener(new MyTextWatcher(recipientEmail));
        recipientPhone.addTextChangedListener(new MyTextWatcher(recipientPhone));
        Intent intent = getIntent();
        voucherHelper = (VoucherHelper) intent.getSerializableExtra("voucherHelper");
        tobeEditedRecipient = (Recipient)intent.getSerializableExtra("recipient");
        recipientFullName.setText(tobeEditedRecipient.RecipientFullName);
        recipientEmail.setText(tobeEditedRecipient.RecipientEmail);
        recipientPhone.setText(tobeEditedRecipient.RecipientPhone);
        setTitle("");
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recipientoptions, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_save_recipient:
                EditRecipient();

                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private boolean validateFullName(){
        String strFullName = recipientFullName.getText().toString();
        if (strFullName.equals("") || strFullName.equals(null)) {
            recipientFullNameLayout.setError("Full Name is required");
            recipientFullNameLayout.requestFocus();
            recipientFullNameLayout .setErrorEnabled(true);
            return false;
        } else {
            recipientFullNameLayout.setError(null);
        }

        return true;
    }
    private boolean validateEmail(){
        String strEmail = recipientEmail.getText().toString();
        String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        boolean validateEmail = strEmail.matches(EMAIL_REGEX);
        if (strEmail.equals("") || strEmail.equals(null)) {
            recipientEmailLayout.setError("Email is required");
            recipientEmailLayout.requestFocus();
            recipientEmailLayout .setErrorEnabled(true);
            return false;
        }else if(!validateEmail) {
            recipientEmailLayout.setError("Email is not in the right format");
            recipientEmailLayout.requestFocus();
            recipientEmailLayout.setErrorEnabled(true);
            return false;
        } else {
            recipientEmailLayout.setError(null);
        }

        return true;
    }
    private boolean validatePhoneNumber(){
        String strPhone = recipientPhone.getText().toString();
        if (strPhone.equals("") || strPhone.equals(null)) {
            recipientPhoneLayout.setError("Phone Number is required");
            recipientPhoneLayout.requestFocus();
            recipientPhoneLayout .setErrorEnabled(true);
            return false;

        } else {
            recipientPhoneLayout.setError(null);
        }

        return true;
    }
    public void EditRecipient(){
        if (validateEmail() && validateFullName() && validatePhoneNumber()){
            String fullName = recipientFullName.getText().toString();
            String email = recipientEmail.getText().toString();
            String phoneNumber = recipientPhone.getText().toString();
            tobeEditedRecipient.RecipientFullName = fullName;
            tobeEditedRecipient.RecipientEmail = email;
            tobeEditedRecipient.RecipientPhone = phoneNumber;
            Recipient.Edit(tobeEditedRecipient, EditRecipientActivity.this);
                if (voucherHelper != null){
                    Intent intent = new Intent(this, RecipientDetailsActivity.class);
                    intent.putExtra("voucherHelper", (Serializable) voucherHelper);
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(this, ProfileActivity.class);
                    intent.putExtra("dest",true);
                    startActivity(intent);
                }
            }

    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.RecipientEmail:
                    validateEmail();
                    break;
                case R.id.RecipientFullName:
                    validateFullName();
                    break;
                case R.id.RecipientPhone:
                    validatePhoneNumber();
                    break;
            }
        }
    }
}
