package com.suregifts.sureremit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.*;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OrderSummaryActivity extends AppCompatActivity {
    private DrawerLayout menuDrawer;
    VoucherHelper voucherHelper;
    Toolbar toolbar;
    ImageView merchantImageView, savedCardProvider;
    TextView merchantName, merchantCategory, recipientName, recipientPhone, voucherAmount, voucherAmountRemit, voucherAmountDollars, savedCardNumber;
    TextView toolBarTitle;
    ImageView stepperImage;
    MaskedFontText cardNumber, expiryDate, ccv;
    TextInputLayout cardNumberLayout, expiryDateLayout, ccvLayout;
    FontButton makePaymentButton;
    ProgressDialog progressDialog;
    LinearLayout cardLayout, savedCardLayout;
    CheckBox saveCardCheck;
    StripePaymentHelper paymentHelper;
    CardDetail savedCard = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        voucherHelper = (VoucherHelper) intent.getSerializableExtra("voucherHelper");
        if (voucherHelper.optype.equals("Voucher")){
            setTheme(R.style.VoucherTheme);
        }
        else if(voucherHelper.optype.equals("Airtime")){
            setTheme(R.style.Airtime);
        }
        else{
            setTheme(R.style.PayBills);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        toolbar = (Toolbar)findViewById(R.id.GenToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        menuDrawer = (DrawerLayout)findViewById(R.id.menuDrawer);
        merchantImageView = (ImageView)findViewById(R.id.VoucherMerchantImage);
        merchantName = (TextView)findViewById(R.id.VoucherMerchantName);
        merchantCategory = (TextView)findViewById(R.id.VoucherMerchantCategory);
        recipientName = (TextView)findViewById(R.id.VoucherRecipientName);
        recipientPhone = (TextView)findViewById(R.id.VoucherRecipientPhone);
//        voucherAmount = (TextView)findViewById(R.id.VoucherAmount);
        voucherAmountDollars = (TextView)findViewById(R.id.VoucherAmountPayable);
        voucherAmountRemit = (TextView)findViewById(R.id.VoucherAmountInRemit);
        toolBarTitle = (TextView)findViewById(R.id.SendVoucherTitleText);
        stepperImage = (ImageView)findViewById(R.id.StepperImage);
        cardNumber = (MaskedFontText)findViewById(R.id.CardNumber);
        expiryDate = (MaskedFontText)findViewById(R.id.CardExpiryDate);
        ccv = (MaskedFontText)findViewById(R.id.CardCCV);
        cardNumberLayout = (TextInputLayout)findViewById(R.id.CardNumberLayout);
        expiryDateLayout = (TextInputLayout)findViewById(R.id.CardExpiryDateLayout);
        makePaymentButton = (FontButton)findViewById(R.id.MakePaymentButton);
        makePaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MakePayment(v);
            }
        });
        ccvLayout = (TextInputLayout)findViewById(R.id.CardCCVLayout);
        cardLayout = (LinearLayout)findViewById(R.id.CardLayout);
        savedCardLayout = (LinearLayout)findViewById(R.id.SavedCardLayout);
        saveCardCheck = (CheckBox)findViewById(R.id.SaveCardCheckbox);
        savedCardNumber = (TextView)findViewById(R.id.SavedCardNumber);
        savedCardProvider = (ImageView)findViewById(R.id.SavedCardProvider);
        setTitle("");
        if (voucherHelper.optype.equals("Voucher")){
            toolBarTitle.setText("Pay");
            stepperImage.setImageResource(R.drawable.voucherstepperfour);
            recipientPhone.setText(voucherHelper.getRecipient_email());

        }
        else if(voucherHelper.optype.equals("Airtime")){
            toolBarTitle.setText("Pay");
            stepperImage.setImageResource(R.drawable.airtimestepperthree);
            recipientPhone.setText(voucherHelper.getRecipient_phone());

        }
        else{
            toolBarTitle.setText("Pay");
            stepperImage.setImageResource(R.drawable.billstepperfour);

        }
        if(voucherHelper != null){
            merchantName.setText(voucherHelper.getMerchant());
            merchantCategory.setText(voucherHelper.getCategory());
            recipientName.setText(voucherHelper.getRecipient_name());

//            if(voucherHelper.getCountry().equals("NG")){ voucherAmount.setText("₦ " + String.valueOf(voucherHelper.getAmount())); }
//            else if(voucherHelper.getCountry().equals("KE")){voucherAmount.setText("Ksh " + String.valueOf(voucherHelper.getAmount()));}
//            else{ voucherAmount.setText("FRW " + String.valueOf(voucherHelper.getAmount())); }
            voucherAmountDollars.setText("$ " + String.valueOf(voucherHelper.getAmount_payable()));
            voucherAmountRemit.setText("RMT " + String.valueOf(Double.parseDouble(voucherHelper.getAmount_payable()) * 50));
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageUrl = voucherHelper.getMerchant_picture();
            viewHolder.imageView = merchantImageView;
            new DownloadAsyncTask(viewHolder).execute(viewHolder);
        }
        progressDialog = new ProgressDialog(this);
        GetUserData();

        if (CardDetail.GetCardDetails(this) != null) {
            savedCard = CardDetail.GetCardDetails(this);
            cardLayout.setVisibility(View.GONE);
            savedCardLayout.setVisibility(View.GONE);
            savedCardNumber.setText(savedCard.CardNumber);
            if (savedCard.CardNumber.startsWith("5")){
                savedCardProvider.setImageResource(R.drawable.mastercard);
            }
            else if (savedCard.CardNumber.startsWith("4")){
                savedCardProvider.setImageResource(R.drawable.visa);
            }
        }
        else{
            cardLayout.setVisibility(View.GONE);
            savedCardLayout.setVisibility(View.GONE);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menuoptions, menu);
        return true;
    }
    public void CloseNavigationDrawer(View view){
        try {
            menuDrawer.closeDrawer(GravityCompat.START);
        }
        catch(Exception ex){

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_drawer:
                menuDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_history:
                Intent intent = new Intent(this, OrderHistoryActivity.class);
                startActivity(intent);
                break;
            default:
                Intent myIntent = new Intent(getApplicationContext(), MenuActivity.class);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void Logout(View view){
        File file = new File(getFilesDir() + "/sureremit_user_cache.txt");
        file.delete();
        Intent intent = new Intent(this, MainActivity.class);
        ComponentName cn = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(cn);
        startActivity(mainIntent);
    }
    public void About(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }
    public void Terms(View view){
        Intent intent = new Intent(this, TandCActivity.class);
        startActivity(intent);
    }
    public void ToMenu(View view){
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    public void Privacy(View view){
        Intent intent = new Intent(this, PrivacyAcivity.class);
        startActivity(intent);
    }
    public void Profile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
    class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.CardNumber:
                    validateCardNumber();
                    break;
                case R.id.CardCCV:
                    validateCCV();
                    break;
                case R.id.CardExpiryDate:
                    validateExpiryDate();
                    break;
            }
        }
    }
    private boolean validateCardNumber(){
        String cardnumber = cardNumber.getUnmaskedText();
        if (cardnumber.equals("") || cardnumber.equals(null)) {
            cardNumberLayout.setError("Card Number is required");
            cardNumberLayout.requestFocus();
            cardNumberLayout.setErrorEnabled(true);
            return false;
        } else {
            cardNumberLayout.setError(null);
        }

        return true;
    }
    private boolean validateCCV(){
        String ccvtext = ccv.getText().toString();
        if (ccvtext.equals("") || ccvtext.equals(null)) {
            ccvLayout.setError("CCV is required");
            ccvLayout.requestFocus();
            ccvLayout.setErrorEnabled(true);
            return false;
        } else {
            ccvLayout.setError(null);
        }

        return true;
    }
    private boolean validateExpiryDate(){
        String expirydate = expiryDate.getUnmaskedText().toString();
        if (expirydate.equals("") || expirydate.equals(null)) {
            expiryDateLayout.setError("Expiry Date is required");
            expiryDateLayout.requestFocus();
            expiryDateLayout.setErrorEnabled(true);
            return false;
        } else {
            expiryDateLayout.setError(null);
        }

        return true;
    }
    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        private int position;
        public DownloadAsyncTask(ViewHolder viewHolder)
        {

        }
        @Override
        protected  ViewHolder doInBackground(ViewHolder... params){
            ViewHolder viewHolder = params[0];

            try{
                URL imageUrl = new URL(viewHolder.imageUrl);
                viewHolder.bitmap = BitmapFactory.decodeStream(imageUrl.openStream());
            }catch(IOException e){
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }
        @Override
        protected void onPostExecute(ViewHolder result){
            if (result.bitmap == null){
                result.imageView.setImageResource(android.R.color.transparent);
            }else{
                result.imageView.setImageBitmap(result.bitmap);
            }

        }
    }
    private static class ViewHolder {
        ImageView imageView;
        TextView name;
        String datetime;
        String imageUrl;
        Bitmap bitmap;
        int id;
    }
    public void MakePayment(View v){
        new SendSuccessEmail().execute();
        DisplaySuccessPopUp("Payment Accepted", "You've Successfully sent a $" + voucherHelper.getAmount_payable() + " " +
                voucherHelper.getMerchant() + " " + voucherHelper.getOptype() + " to " + voucherHelper.recipient_name);
    }
    public class SendSuccessEmail extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid){
            try {

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            String merchantUrl = "http://sureremit.johnoke.com/api/Mail?email=" + voucherHelper.recipient_email + "&name=" + voucherHelper.name + "&amount=" + voucherHelper.amount;
            request = new Request.Builder()
                    .url(merchantUrl)
                    .build();

            Response response;
            try{
                response = client.newCall(request).execute();

            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return null;
        }
    }
//    public void MakePayment(View v){
//
//        int month;
//        int year;
//        try{
//
//            progressDialog.setIndeterminate(true);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Validating Card");
//            progressDialog.show();
//
//            Card card;
//            if (savedCard != null){
//                card = new Card(savedCard.CardNumber, Integer.valueOf(savedCard.ExpiryMonth), Integer.valueOf(savedCard.ExpiryYear), savedCard.CCV);
//            }
//            else{
//                String strCardNumber = cardNumber.getUnmaskedText().toString();
//                String strCCV = ccv.getUnmaskedText().toString();
//                String strExpiryDate = expiryDate.getText().toString();
//                String[] expirymonthyear = strExpiryDate.split("/");
//                month = Integer.parseInt(expirymonthyear[0].replaceAll(" ", ""));
//                year = Integer.parseInt(expirymonthyear[1].replaceAll(" ", ""));
//                card = new Card(strCardNumber, month, year, strCCV);
//            }
//
//            if (!card.validateCard()){
//                progressDialog.dismiss();
//                final Dialog dialog = new Dialog(OrderSummaryActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.errordialog);
//                TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
//                TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
//                ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
//                LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
//                popupImage.setImageResource(R.drawable.error);
//                popupLabel.setText("Invalid Card Details");
//                popupmessage.setText("The Card Details you provided is incorrect, Please check and try again.");
//                okbutton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//            else{
//                progressDialog.setMessage("Please Wait");
//                try{
//                    Stripe stripe = new Stripe(null);
//                    stripe.createToken(card, new TokenCallback() {
//                        @Override
//                        public void onError(Exception error) {
//                            DisplayErrorPopUp("Something went Wrong", "Payment was unsuccessful. Your credit card was not charged");
//                        }
//
//                        @Override
//                        public void onSuccess(Token token) {
//                            paymentHelper = new StripePaymentHelper(token.getId(),Double.parseDouble(voucherHelper.amount_payable) * 100);
//                            new AsyncTaskChargeStripe().execute();
//                        }
//                    });
//                }catch(Exception ex){
//                    DisplayErrorPopUp("Something went Wrong", "Payment was unsuccessful. Your credit card was not charged");
//                }
//            }
//        }catch(Exception ex){
//            DisplayErrorPopUp("Something Went Wrong", "We couldn't validate your card, Please try again later");
//            ex.printStackTrace();
//        }
//
//        //Toast.makeText(this, strUnmaskedText, Toast.LENGTH_LONG).show();
//    }
    public class AsyncTaskDeliverGiftCard extends AsyncTask<String, Void, String>{
        @Override
        protected void onPostExecute(String result) {
            if (result != null){
                try{
                    JSONObject response = new JSONObject(result);
                    int statusCode = response.getInt("statusCode");
                    String message = response.getString("message");
                    //DisplayErrorPopUp("Response", result);
                    if (statusCode == 200){
                        if (saveCardCheck.isChecked()){
                            String strCardNumber = cardNumber.getUnmaskedText().toString();
                            String strCCV = ccv.getUnmaskedText().toString();
                            String strExpiryDate = expiryDate.getText().toString();
                            String[] expirymonthyear = strExpiryDate.split("/");
                            SaveCard(new CardDetail(strCardNumber, strCCV, expirymonthyear[0], expirymonthyear[1]));
                        }
                        Merchant merchant = new Merchant(voucherHelper.merchant_id, voucherHelper.merchant, voucherHelper.merchant_picture, voucherHelper.category_id,
                                                         "", "");
                        merchant.setLabel(voucherHelper.merchant_label);
                        merchant.setCode(voucherHelper.code);
                        merchant.setBillPackages(voucherHelper.getBillPackages());
                        merchant.setCountry(voucherHelper.country);
                        try{
                            if (voucherHelper.optype.equals("Voucher")){
                                Merchant.AddVoucherHistory(merchant, getApplicationContext());
                            }
                            else if(voucherHelper.optype.equals("Airtime")){
                                Merchant.AddAirtimeHistory(merchant, getApplicationContext());
                            }
                            else{
                                Merchant.AddBillHistory(merchant, getApplicationContext());
                            }
                        }catch (Exception ex){

                        }

                        DisplaySuccessPopUp("Payment Accepted", "You've Successfully sent a $" + voucherHelper.getAmount_payable() + " " +
                                voucherHelper.getMerchant() + " " + voucherHelper.getOptype() + " to " + voucherHelper.recipient_name);

                    }
                    else{
                        DisplayErrorPopUp("Operation Failed", message);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                    DisplayErrorPopUp("Operation Failed","Something went wrong while processing this request, Please try again later");
                }
            }
            else {
                DisplayErrorPopUp("Operation Failed", "We are unable to process ");
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(500, TimeUnit.SECONDS)
                        .writeTimeout(500, TimeUnit.SECONDS)
                        .connectTimeout(500, TimeUnit.SECONDS)
                        .build();
                Gson gson = new GsonBuilder().create();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String chargeJSON = "";
                String url = "";
                if (voucherHelper.optype.equals("Voucher")){
                    VoucherRequest voucherRequest = new VoucherRequest(voucherHelper.location, voucherHelper.recipient_name, voucherHelper.recipient_phone,
                            voucherHelper.recipient_email, voucherHelper.merchant, String.valueOf(voucherHelper.merchant_id), String.valueOf(Double.valueOf(voucherHelper.amount_payable) * 100),
                            voucherHelper.amount, voucherHelper.email, voucherHelper.payment_id, voucherHelper.merchant_picture);
                    voucherRequest.name = voucherHelper.name;
                    voucherRequest.category = voucherHelper.category;
                    chargeJSON = gson.toJson(voucherRequest);
                    url = "http://sureremit.co/api/voucher/send";
                }
                else if(voucherHelper.optype.equals("Utility")){
                    BillRequest billRequest = new BillRequest(voucherHelper.location, voucherHelper.merchant, String.valueOf(voucherHelper.merchant_id),
                                                               voucherHelper.recipient_phone, voucherHelper.package_number, String.valueOf(Double.valueOf(voucherHelper.amount_payable) * 100),
                                                               voucherHelper.amount, voucherHelper.email, voucherHelper.payment_id, voucherHelper.merchant_picture);
                    billRequest.name = voucherHelper.name;
                    billRequest.category = voucherHelper.category;
                    chargeJSON = gson.toJson(billRequest);
                    url = "http://sureremit.co/api/bill/pay";
                }
                else{
                    BillRequest billRequest = new BillRequest(voucherHelper.location, voucherHelper.merchant, String.valueOf(voucherHelper.code),
                            voucherHelper.recipient_phone, voucherHelper.package_number, String.valueOf(Double.valueOf(voucherHelper.amount_payable) * 100),
                            voucherHelper.amount, voucherHelper.email, voucherHelper.payment_id, voucherHelper.merchant_picture);
                    billRequest.name = voucherHelper.name;
                    billRequest.category = voucherHelper.category;
                    chargeJSON = gson.toJson(billRequest);
                    url = "http://sureremit.co/api/airtime/send";
                }
                RequestBody body = RequestBody.create(JSON, chargeJSON);
                Log.e("Json", chargeJSON);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.e("Response", result);
                return result;

            }catch (Exception ex){
                return null;
            }
        }
    }
    public class AsyncTaskChargeStripe extends AsyncTask<String, Void, String>{
        @Override
        protected void onPostExecute(String result) {
            if (result != null){
                try{
                    JSONObject stripeResponse = new JSONObject(result);
                    int statusCode = stripeResponse.getInt("statusCode");
                    if (statusCode == 200){
                        String paymentId = stripeResponse.getString("paymentId");
                        voucherHelper.setPayment_id(paymentId);
                        if (voucherHelper.country.equals("NG")) voucherHelper.setLocation("nigeria");
                        else if (voucherHelper.country.equals("KE")) voucherHelper.setLocation("kenya");
                        else voucherHelper.setLocation("rwanda");
                        new AsyncTaskDeliverGiftCard().execute();
                    }

                    else{
                        String message = stripeResponse.getString("message");
                        DisplayErrorPopUp("Something went wrong", message);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                    DisplayErrorPopUp("Something Went Wrong", "Payment was unsuccessful, Please try again later");
                }
            }
            else{
                DisplayErrorPopUp("Something Went Wrong", "Payment was unsuccessful, Please try again later");
            }

        }

        @Override
        protected String doInBackground(String... params) {
            try{
                OkHttpClient client = new OkHttpClient.Builder()
                        .readTimeout(200, TimeUnit.SECONDS)
                        .writeTimeout(200, TimeUnit.SECONDS)
                        .connectTimeout(200, TimeUnit.SECONDS)
                        .build();
                Gson gson = new GsonBuilder().create();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                String chargeJSON = gson.toJson(paymentHelper);
                RequestBody body = RequestBody.create(JSON, chargeJSON);
                Request request = new Request.Builder()
                        .url("http://sureremit.co/api/charge")
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                return result;

            }catch (Exception ex){
                return null;
            }

        }
    }
    public void DisplayErrorPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(OrderSummaryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        popupImage.setImageResource(R.drawable.error);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void DisplaySuccessPopUp(String Title, String Message){
        progressDialog.dismiss();
        final Dialog dialog = new Dialog(OrderSummaryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.errordialog);
        TextView popupLabel = (TextView)dialog.findViewById(R.id.PopUpLabel);
        TextView popupmessage = (TextView)dialog.findViewById(R.id.PopUpMesage);
        ImageView popupImage = (ImageView)dialog.findViewById(R.id.PopUpImage);
        LinearLayout okbutton = (LinearLayout)dialog.findViewById(R.id.PopUpOkayButton);
        TextView dismissButton = (TextView)dialog.findViewById(R.id.PopUpDismissButton);
        dismissButton.setText("Back to Main Menu");
        popupImage.setImageResource(R.drawable.check);
        popupLabel.setText(Title);
        popupmessage.setText(Message);
        okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        dialog.show();
    }
    public void GetUserData()
    {
        try{
            String rawdata = new Scanner(new File(getFilesDir() + "/sureremit_user_cache.txt")).nextLine();
            JSONObject obj = new JSONObject(rawdata);
            voucherHelper.setEmail(obj.getString("Email"));
            String FirstName = obj.getString("FirstName");
            String LastName = obj.getString("LastName");
            voucherHelper.name = FirstName + " " + LastName;
        }
        catch (Exception ex){

        }

    }
    public void SaveCard(CardDetail cardDetail){
        try{
            if (CardDetail.Add(cardDetail, this)) {
            }
            else{
                Log.e("Error Adding Card:","Unable to add Card check log");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }


    }
    public void EditCard(View view){
        Intent intent = new Intent(this, EditCardActivity.class);
        intent.putExtra("voucherHelper", (Serializable) voucherHelper);
        startActivity(intent);
    }
}
