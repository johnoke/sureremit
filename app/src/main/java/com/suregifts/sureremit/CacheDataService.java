package com.suregifts.sureremit;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by SUREGIFTS-DEVPC on 7/29/2016.
 */
public class CacheDataService extends Service {
    public static final long NOTIFY_INTERVAL = 24 * 60* 60 * 1000; //10 seconds
    String airtimesJson = "";
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    public List<ServiceSupport> supports;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        if (mTimer != null){
            mTimer.cancel();
        }else{
            mTimer = new Timer();
        }
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }
    class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.e("Started", "Service Started");
                    InitializeSupportLists();
                    for(final ServiceSupport support : supports){
                        new FetchAllAirtimes(support).execute(support);
                       // Toast.makeText(getApplicationContext(), support.Response, Toast.LENGTH_LONG).show();
                    }

                }
            });
        }
        private String getDateTime() {
            // get date time in custom format
            SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
            return sdf.format(new Date());
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    public class ServiceSupport {
        public String Country;
        public String Url;
        public String FileName;
        public String Response;
        public ServiceSupport(String country, String url, String fileName){
            this.Country = country;
            this.Url = url;
            this.FileName = fileName;
            this.Response = "";
        }
    }
    public class FetchAllAirtimes extends AsyncTask<ServiceSupport, ServiceSupport, ServiceSupport> {
        public ServiceSupport support;
        public FetchAllAirtimes(ServiceSupport support){
            this.support = support;
        }
        @Override
        protected void onPostExecute(ServiceSupport support){
            try {
                FileOutputStream fileOutputStream =  getApplicationContext().openFileOutput(support.FileName, Context.MODE_WORLD_WRITEABLE);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                outputStreamWriter.write(support.Response);
                outputStreamWriter.flush();
                outputStreamWriter.close();

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        @Override
        protected ServiceSupport doInBackground(ServiceSupport... params) {
            ServiceSupport support = params[0];
            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();
            Request request;
            request = new Request.Builder()
                    .url(support.Url)
                    .build();
            Response response;
            try{
                response = client.newCall(request).execute();
                support.Response = response.body().string();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            return support;
        }
    }
    public void InitializeSupportLists(){
        supports = new ArrayList<ServiceSupport>();
        ServiceSupport airtimeSupportNG = new ServiceSupport("NG", "http://cms.suregifts.com.ng/api/utilities/phone", "sureremit_airtime_cache_NG.txt");
        ServiceSupport airtimeSupportKE = new ServiceSupport("KE", "http://cms.suregifts.co.ke/api/utilities/phone", "sureremit_airtime_cache_KE.txt");
        ServiceSupport airtimeSupportRW = new ServiceSupport("RW", "http://cms.suregifts.rw/api/utilities/phone", "sureremit_airtime_cache_RW.txt");
        ServiceSupport billSupportNG = new ServiceSupport("NG", "http://cms.suregifts.com.ng/api/utilities", "sureremit_bills_cache_NG.txt");
        ServiceSupport billSupportKE = new ServiceSupport("KE", "http://cms.suregifts.co.ke/api/utilities", "sureremit_bills_cache_KE.txt");
        ServiceSupport billSupportRW = new ServiceSupport("RW", "http://cms.suregifts.rw/api/utilities", "sureremit_bills_cache_RW.txt");
        ServiceSupport voucherSupportNG = new ServiceSupport("NG", "http://cms.suregifts.com.ng/api/giftcard/merchantsandcategory", "sureremit_merchants_cache_NG.txt");
        ServiceSupport voucherSupportKE = new ServiceSupport("KE", "http://cms.suregifts.co.ke/api/giftcard/merchantsandcategory", "sureremit_merchants_cache_KE.txt");
        supports.add(voucherSupportNG);
        supports.add(voucherSupportKE);
        supports.add(billSupportRW);
        supports.add(billSupportKE);
        supports.add(billSupportNG);
        supports.add(airtimeSupportRW);
        supports.add(airtimeSupportKE);
        supports.add(airtimeSupportNG);

    }
}
