package com.suregifts.sureremit;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SUREGIFTS-DEVPC on 7/7/2016.
 */
public class Bill implements Serializable {
    public int BillId;
    public String BillName;
    public String Label;
    public String PictureUrl;
    public List<BillPackage> Packages;
}
