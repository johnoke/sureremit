package com.suregifts.sureremit;

/**
 * Created by SUREGIFTS-DEVPC on 6/6/2016.
 */
public class BillRequest {
    public String location;
    public String service;
    public String service_id;
    public String recipient_phone;
    public String reference_number;
    public String amount_payable;
    public String amount;
    public String email;
    public String payment_id;
    public String merchant_image;
    public String category;
    public String name;
    public BillRequest(String location, String service, String service_id, String recipient_phone,
                       String reference_number, String amount_payable, String amount, String email, String payment_id, String merchant_image)
    {
        this.location = location;
        this.service = service;
        this.service_id = service_id;
        this.recipient_phone = recipient_phone;
        this.reference_number = reference_number;
        this.amount = amount;
        this.amount_payable = amount_payable;
        this.email = email;
        this.payment_id = payment_id;
        this.merchant_image = merchant_image;
    }
}
